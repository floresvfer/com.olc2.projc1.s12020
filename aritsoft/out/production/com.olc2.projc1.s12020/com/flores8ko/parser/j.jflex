package com.flores8ko.parser;

import java_cup.runtime.Symbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import  com.flores8ko.aritlanguage.operations.*;
import com.flores8ko.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


%%

%class T4Scanner
%cupsym T4SYM
%cup
%public
%unicode
%line
%column
%char

%ignorecase

%{

    StringBuffer string = new StringBuffer();

    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn, yytext());
        //return new Symbol(type, yyline, yycolumn, new Coordenada(yytext(), yyline + 1, yycolumn + 1) );
    }
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }

    private String stringContent(String string){
        return string.substring(1, string.length() - 1);
    }

%}

double = [0-9]+[.][0-9]+
integer         = [0-9]+
char   = ['][a-zA-Z0-9][']
string = \"[^\"]*\"
id = [:jletter:] [:jletterdigit:]*

LineTerminator  = \r|\n|\r\n
InputCharacter = [^\r\n]

WhiteSpace = {LineTerminator} | [ \t\f]

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment} |
          {DocumentationComment}

TraditionalComment = "#*" [^}] ~"*#"
EndOfLineComment = "#" {InputCharacter}* {LineTerminator}?
DocumentationComment = "#*" [^}] ~"*#"


%state STRI




%%

<YYINITIAL>{


    "+"     { return symbol(T4SYM.PLUS); }
    "^"     { return symbol(T4SYM.POW); }
    "-"     { return symbol(T4SYM.MINUS); }
    "/"     { return symbol(T4SYM.SLASH); }
    "*"     { return symbol(T4SYM.TIMES); }
    "%"     { return symbol(T4SYM.MOD); }


    "=="    { return symbol(T4SYM.EQ); }
    "!="    { return symbol(T4SYM.DIF); }
    "<"     { return symbol(T4SYM.MIN); }
    "<="    { return symbol(T4SYM.MINEQ); }
    ">"     { return symbol(T4SYM.MAY); }
    ">="    { return symbol(T4SYM.MAYEQ); }
    ") =>"    { return symbol(T4SYM.LAMDA); }


    "&"    { return symbol(T4SYM.AND); }
    "|"    { return symbol(T4SYM.OR); }
    "!"     { return symbol(T4SYM.NOT); }

    "="     { return symbol(T4SYM.EQUAL); }
    ";"     { return symbol(T4SYM.SEMICOLON); }
    ":"     { return symbol(T4SYM.COLON); }
    "?"     { return symbol(T4SYM.ASK); }
    "."     { return symbol(T4SYM.DOT);  }
    "c" { return symbol(T4SYM.C); }
    "list" { return symbol(T4SYM.LIST); }
    "matrix" { return symbol(T4SYM.MATRIX); }
    "nrow" { return symbol(T4SYM.NROW); }
    "ncol" { return symbol(T4SYM.NCOL); }
    "length" { return symbol(T4SYM.LENGTH); }
    "mean" { return symbol(T4SYM.MEAN); }
    "mode" { return symbol(T4SYM.MODE); }
    "median" { return symbol(T4SYM.MEDIAN); }

    "pie"   { return symbol(T4SYM.PIE); }
    "barplot" { return symbol(T4SYM.BARPLOT); }
    "plot" { return symbol(T4SYM.PLOT); }
    "hist" { return symbol(T4SYM.HIST); }

    "print" { return symbol(T4SYM.PRINT); }
    "stringlength" { return symbol(T4SYM.STRINGLENGHT); }
    "typeof" { return symbol(T4SYM.TYPEOF); }
    "tolowercase" { return symbol(T4SYM.TOLOWERCASE); }
    "touppercase" { return symbol(T4SYM.TOUPPERCASE); }
    "round" { return symbol(T4SYM.ROUND); }
    "trunk" { return symbol(T4SYM.TRUNK); }
    "true" { return symbol(T4SYM.TRUE); }
    "false" { return symbol(T4SYM.FALSE); }
    "null" { return symbol(T4SYM.NULL); }
    "("     { return symbol(T4SYM.P_OPEN); }
    ")"     { return symbol(T4SYM.P_CLOSE); }
    "["     { return symbol(T4SYM.C_OPEN); }
    "]"     { return symbol(T4SYM.C_CLOSE); }
    "{"     { return symbol(T4SYM.LL_OPEN); }
    "}"     { return symbol(T4SYM.LL_CLOSE); }
    ","     { return symbol(T4SYM.COMMA); }

    "++"    { return symbol(T4SYM.ADD); }
    "--"    { return symbol(T4SYM.SUB); }

    "+="    { return symbol(T4SYM.READD); }
    "-="    { return symbol(T4SYM.RESUB); }
    "*="    { return symbol(T4SYM.REMUL); }
    "/="    { return symbol(T4SYM.REDIV); }

    "if"    { return symbol(T4SYM.IF); }
    "else"  { return symbol(T4SYM.ELSE); }

    "while"  { return symbol(T4SYM.WHILE); }
    "do" { return symbol(T4SYM.DO); }

    "for"    { return symbol(T4SYM.FOR); }
    "in" { return symbol(T4SYM.IN); }

    "switch" { return symbol(T4SYM.SELECT); }
    "case"  { return symbol(T4SYM.CASO); }
    "default" { return symbol(T4SYM.DEFAULT); }

    "break" { return symbol(T4SYM.BREAK); }
    "return" { return symbol(T4SYM.RETURN); }
    "continue" { return symbol(T4SYM.CONTINUE); }

    "function" { return symbol(T4SYM.FUNCION); }

    {integer}       { return symbol(T4SYM.INTEGER, Integer.valueOf(yytext())); }
    {char}          { return symbol(T4SYM.CHAR, yytext()); }
    \"              { string.setLength(0); yybegin(STRI); }
//    {string}        { return symbol(T4SYM.STRING, yytext()); }
    {double}        { return symbol(T4SYM.NUMERIC, Double.valueOf(yytext())); }
    {id}       { return symbol(T4SYM.ID, yytext()); }

    {Comment}       {}
    {WhiteSpace}    {}
}


    <STRI> {
      \"                             { yybegin(YYINITIAL);
                                       return symbol(T4SYM.STRING,
                                       string.toString()); }
      [^\n\r\"\\]+                   { string.append( yytext() ); }
      \\t                            { string.append('\t'); }
      \\n                            { string.append('\n'); }

      \\r                            { string.append('\r'); }
      \\\"                           { string.append('\"'); }
      \\                             { string.append('\\'); }
    }
  [^]                              { throw new Error("Illegal character <"+
                                                        yytext()+">"); }

