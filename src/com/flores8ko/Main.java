package com.flores8ko;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static class TextEditorDemo extends JFrame {
        TextEditorDemo(){
            JPanel cp = new JPanel(new BorderLayout());

            RSyntaxTextArea textArea = new RSyntaxTextArea(20, 60);
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
            textArea.setCodeFoldingEnabled(true);
            RTextScrollPane sp = new RTextScrollPane(textArea);

            cp.setSize(500,500);
            cp.add(sp);




            setSize(600, 200);
            add(cp);
            setTitle("Text Editor Demo");
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            pack();
            setLocationRelativeTo(null);
        }
    }




    public static void main(String[] args)  {
        SwingUtilities.invokeLater(() -> new TextEditorDemo().setVisible(true));
	// write your code here
//        try {
//            analize(readFile(false));
//            analizejavacc(readFile(true));
//            System.out.println();
//        }catch (Exception e){
//            System.out.println(e);
//            e.printStackTrace();
//        }
        /*catch (IOException e){
            System.out.println(e);
        }*/
    }

    public static String readFile(boolean descendent) throws IOException {
        if(descendent)
            return Files.readString(Paths.get("files/tests2.txt"), StandardCharsets.UTF_8);
        return Files.readString(Paths.get("files/tests.txt"), StandardCharsets.UTF_8);
    }
}
