package com.flores8ko.aritlanguage;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer.*;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class Algebra {

    private static VECTOR AlgebraOp(String Algebra, VECTOR lf, Cntnr rt) throws Utils.SemanticException {
        List<Cntnr> vals = new ArrayList<>();
        for (var val : lf.GetValueList()) {
            Cntnr v = null;
            switch (Algebra.toLowerCase()) {
                case "sumar":
                    v = Suma(val, rt);
                    break;
                case "restar":
                    v = Resta(val, rt);
                    break;
                case "multiplicar":
                    v = Multiplicacion(val, rt);
                    break;
                case "dividir":
                    v = Division(val, rt);
                    break;
                case "potenciar":
                    v = Potencia(val, rt);
                    break;
                case "modular":
                    v = Modulo(val, rt);
                    break;
            }
            vals.add(new Reference(v));
        }
        return new VECTOR(vals);
    }

    private static VECTOR AlgebraOp(String Algebra, VECTOR lf, VECTOR rt) throws Utils.SemanticException {
        var lfValues = lf.GetValueList();
        var rtValues = rt.GetValueList();
        if (lfValues.size() != rtValues.size())
            throw new Utils.SemanticException("Para Operar dos vectores deben ser del mismo tamaño.");
        List<Cntnr> vals = new ArrayList<>();
        for (var i = 0; i < lf.GetValueList().size(); i++){
            Cntnr v = null;
            switch (Algebra.toLowerCase()) {
                case "sumar":
                    v = Suma(lfValues.get(i), rtValues.get(i));
                    break;
                case "restar":
                    v = Resta(lfValues.get(i), rtValues.get(i));
                    break;
                case "multiplicar":
                    v = Multiplicacion(lfValues.get(i), rtValues.get(i));
                    break;
                case "dividir":
                    v = Division(lfValues.get(i), rtValues.get(i));
                    break;
                case "potenciar":
                    v = Potencia(lfValues.get(i), rtValues.get(i));
                    break;
                case "modular":
                    v = Modulo(lfValues.get(i), rtValues.get(i));
                    break;
            }
            vals.add(new Reference(v));
        }
        return new VECTOR(vals);
    }

    //region SUMA
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Suma(Cntnr lf, Cntnr rt) throws Utils.SemanticException {

        if (rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if (lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region ENTERO
        if (lf instanceof INTEGER && rt instanceof INTEGER)
            return Sumar((INTEGER) lf, (INTEGER) rt);
        // if (lf instanceof INTEGER && rt instanceof CHAR)
        //     return Sumar((INTEGER) lf, (CHAR) rt);
        // if (lf instanceof CHAR && rt instanceof INTEGER)
        //     return Sumar((CHAR) lf, (INTEGER) rt);
        //endregion
        //region DOUBLE
        if (lf instanceof NUMERIC && rt instanceof INTEGER)
            return Sumar((NUMERIC) lf, (INTEGER) rt);
        if (lf instanceof INTEGER && rt instanceof NUMERIC)
            return Sumar((INTEGER) lf, (NUMERIC) rt);
        if (lf instanceof NUMERIC && rt instanceof NUMERIC)
            return Sumar((NUMERIC) lf, (NUMERIC) rt);
        // if (lf instanceof CHAR && rt instanceof NUMERIC)
        //     return Sumar((CHAR) lf, (NUMERIC) rt);
        // if (lf instanceof NUMERIC && rt instanceof CHAR)
        //     return Sumar((NUMERIC) lf, (CHAR) rt);
        //endregion
        //region STRING
        if (lf instanceof INTEGER && rt instanceof STRING)
            return Sumar((INTEGER) lf, (STRING) rt);
        if (lf instanceof STRING && rt instanceof INTEGER)
            return Sumar((STRING) lf, (INTEGER) rt);
        if (lf instanceof STRING && rt instanceof STRING)
            return Sumar((STRING) lf, (STRING) rt);
        if (lf instanceof BOOL && rt instanceof STRING)
            return Sumar((BOOL) lf, (STRING) rt);
        if (lf instanceof STRING && rt instanceof BOOL)
            return Sumar((STRING) lf, (BOOL) rt);
        // if (lf instanceof CHAR && rt instanceof STRING)
        //     return Sumar((CHAR) lf, (STRING) rt);
        // if (lf instanceof STRING && rt instanceof CHAR)
        //     return Sumar((STRING) lf, (CHAR) rt);
        if (lf instanceof NUMERIC && rt instanceof STRING)
            return Sumar((NUMERIC) lf, (STRING) rt);
        if (lf instanceof STRING && rt instanceof NUMERIC)
            return Sumar((STRING) lf, (NUMERIC) rt);
        // if(lf instanceof STRING && rt instanceof ARRAY)
        //     return Sumar((STRING)lf, (ARRAY)rt);
        // if(lf instanceof ARRAY && rt instanceof STRING)
        //     return Sumar((ARRAY) lf, (STRING) rt);
        //endregion
        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return AlgebraOp("Sumar", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return AlgebraOp("Sumar", (VECTOR) lf, rt);


        throw new Utils.SemanticException("Operacion entre tipos ( {" + lf.typo + "} + {" + lf.typo + "} ) no admitida");
    }

    //region METH
    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Sumar(@NotNull INTEGER lf, @NotNull INTEGER rt) {
        return new INTEGER(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Sumar(@NotNull CHAR lf, @NotNull INTEGER rt) {
        return new INTEGER(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Sumar(@NotNull INTEGER lf, @NotNull CHAR rt) {
        return new INTEGER(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Sumar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt) {
        return new NUMERIC(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Sumar(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Sumar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Sumar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull CHAR rt) {
        return new NUMERIC(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Sumar(@NotNull CHAR lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull INTEGER lf, @NotNull STRING rt) {
        return new STRING(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull STRING lf, @NotNull INTEGER rt) {
        return new STRING(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull BOOL lf, @NotNull STRING rt) {
        return new STRING(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull STRING lf, @NotNull BOOL rt) {
        return new STRING(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull STRING lf, @NotNull STRING rt) {
        return new STRING(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull CHAR lf, @NotNull STRING rt) {
        return new STRING(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull STRING lf, @NotNull CHAR rt) {
        return new STRING(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull STRING rt) {
        return new STRING(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull STRING lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new STRING(lf.GetValue() + rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull STRING lf, @NotNull ARRAY rt) {
        return new STRING(lf.GetValue() + rt.toString());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static STRING Sumar(@NotNull ARRAY lf, @NotNull STRING rt) {
        return new STRING(lf.toString() + rt.GetValue());
    }
    //endregion
    //endregion

    //region RESTA
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Resta(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if (rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if (lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        if (lf instanceof INTEGER && rt instanceof INTEGER)
            return Restar((INTEGER) lf, (INTEGER) rt);
        //if(lf instanceof INTEGER && rt instanceof CHAR)
        //    return Restar((INTEGER) lf, (CHAR) rt);
        //if(lf instanceof CHAR && rt instanceof INTEGER)
        //    return Restar((CHAR) lf, (INTEGER)rt);
        if (lf instanceof INTEGER && rt instanceof NUMERIC)
            return Restar((INTEGER) lf, (NUMERIC) rt);
        if (lf instanceof NUMERIC && rt instanceof INTEGER)
            return Restar((NUMERIC) lf, (INTEGER) rt);
        //if(lf instanceof CHAR && rt instanceof NUMERIC)
        //    return Restar((CHAR) lf, (NUMERIC) rt);
        //if(lf instanceof NUMERIC && rt instanceof CHAR)
        //    return Restar((NUMERIC) lf, (CHAR) rt);
        if (lf instanceof NUMERIC && rt instanceof NUMERIC)
            return Restar((NUMERIC) lf, (NUMERIC) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return AlgebraOp("Restar", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return AlgebraOp("Restar", (VECTOR) lf, rt);

        throw new Utils.SemanticException("Operacion entre tipos ( {" + lf.typo + "} - {" + lf.typo + "} ) no admitida");
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Restar(@NotNull INTEGER lf, @NotNull INTEGER rt) {
        return new INTEGER(lf.GetValue() - rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Restar(@NotNull INTEGER lf, @NotNull CHAR rt) {
        return new INTEGER(lf.GetValue() - rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Restar(@NotNull CHAR lf, @NotNull INTEGER rt) {
        return new INTEGER(lf.GetValue() - rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Restar(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() - rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Restar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt) {
        return new NUMERIC(lf.GetValue() - rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Restar(@NotNull CHAR lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() - rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Restar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull CHAR rt) {
        return new NUMERIC(lf.GetValue() - rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Restar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() - rt.GetValue());
    }
    //endregion

    //region MULTIPLICACION
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Multiplicacion(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if (rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if (lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        if (lf instanceof INTEGER && rt instanceof INTEGER)
            return Multiplicar((INTEGER) lf, (INTEGER) rt);
        //if(lf instanceof INTEGER && rt instanceof CHAR)
        //    return Multiplicar((INTEGER) lf, (CHAR) rt);
        //if(lf instanceof CHAR && rt instanceof INTEGER)
        //    return Multiplicar((CHAR) lf, (INTEGER) rt);
        if (lf instanceof INTEGER && rt instanceof NUMERIC)
            return Multiplicar((INTEGER) lf, (NUMERIC) rt);
        if (lf instanceof NUMERIC && rt instanceof INTEGER)
            return Multiplicar((NUMERIC) lf, (INTEGER) rt);
        //if(lf instanceof NUMERIC && rt instanceof CHAR)
        //    return Multiplicar((NUMERIC) lf, (CHAR) rt);
        //if(lf instanceof CHAR && rt instanceof NUMERIC)
        //    return Multiplicar((CHAR) lf, (NUMERIC) rt);
        if (lf instanceof NUMERIC && rt instanceof NUMERIC)
            return Multiplicar((NUMERIC) lf, (NUMERIC) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return AlgebraOp("Multiplicar", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return AlgebraOp("Multiplicar", (VECTOR) lf, rt);

        throw new Utils.SemanticException("Operacion entre tipos ( {" + lf.typo + "} * {" + lf.typo + "} ) no admitida");
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Multiplicar(@NotNull INTEGER lf, @NotNull INTEGER rt) {
        return new INTEGER(lf.GetValue() * rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Multiplicar(@NotNull INTEGER lf, @NotNull CHAR rt) {
        return new INTEGER(lf.GetValue() * rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Multiplicar(@NotNull CHAR lf, @NotNull INTEGER rt) {
        return new INTEGER(lf.GetValue() * rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Multiplicar(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() * rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Multiplicar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt) {
        return new NUMERIC(lf.GetValue() * rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Multiplicar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull CHAR rt) {
        return new NUMERIC(lf.GetValue() * rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Multiplicar(@NotNull CHAR lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() * rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Multiplicar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() * rt.GetValue());
    }
    //endregion

    //region DIVISION
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Division(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if (rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if (lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        if (lf instanceof INTEGER && rt instanceof INTEGER)
            return Dividir((INTEGER) lf, (INTEGER) rt);
        if (lf instanceof NUMERIC && rt instanceof INTEGER)
            return Dividir((NUMERIC) lf, (INTEGER) rt);
        if (lf instanceof NUMERIC && rt instanceof CHAR)
            return Dividir((NUMERIC) lf, (CHAR) rt);
        if (lf instanceof CHAR && rt instanceof NUMERIC)
            return Dividir((CHAR) lf, (NUMERIC) rt);
        if (lf instanceof NUMERIC && rt instanceof NUMERIC)
            return Dividir((NUMERIC) lf, (NUMERIC) rt);
        if (lf instanceof INTEGER && rt instanceof NUMERIC)
            return Dividir((INTEGER) lf, (NUMERIC) rt);
        if (lf instanceof CHAR && rt instanceof INTEGER)
            return Dividir((CHAR) lf, (INTEGER) rt);
        if (lf instanceof INTEGER && rt instanceof CHAR)
            return Dividir((INTEGER) lf, (CHAR) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return AlgebraOp("Dividir", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return AlgebraOp("Dividir", (VECTOR) lf, rt);

        throw new Utils.SemanticException("Operacion entre tipos ( {" + lf.typo + "} / {" + lf.typo + "} ) no admitida");
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Dividir(@NotNull INTEGER lf, @NotNull INTEGER rt) {
        return new NUMERIC((double) lf.GetValue() / (double) rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Dividir(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt) {
        return new NUMERIC(lf.GetValue() / rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Dividir(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull CHAR rt) {
        return new NUMERIC(lf.GetValue() / rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Dividir(@NotNull CHAR lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() / rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Dividir(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() / rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Dividir(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() / rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Dividir(@NotNull CHAR lf, @NotNull INTEGER rt) {
        return new NUMERIC((double) lf.GetValue() / (double) rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Dividir(@NotNull INTEGER lf, @NotNull CHAR rt) {
        return new NUMERIC((double) lf.GetValue() / (double) rt.GetValue());
    }
    //endregion

    //region POTENCIA
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Potencia(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if (rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if (lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region DOUBLE
        if (lf instanceof INTEGER && rt instanceof NUMERIC)
            return Potenciar((INTEGER) lf, (NUMERIC) rt);
        if (lf instanceof NUMERIC && rt instanceof INTEGER)
            return Potenciar((NUMERIC) lf, (INTEGER) rt);
        //if(lf instanceof NUMERIC && rt instanceof CHAR)
        //    return Potenciar((NUMERIC) lf, (CHAR) rt);
        //if(lf instanceof CHAR && rt instanceof NUMERIC)
        //    return Potenciar((CHAR) lf, (NUMERIC) rt);
        //if(lf instanceof BOOL && rt instanceof NUMERIC)
        //    return Potenciar((BOOL) lf, (NUMERIC)rt);
        //if(lf instanceof NUMERIC && rt instanceof BOOL)
        //    return Potenciar((NUMERIC) lf, (BOOL)rt);
        if (lf instanceof NUMERIC && rt instanceof NUMERIC)
            return Potenciar((NUMERIC) lf, (NUMERIC) rt);
        //endregion
        //region ENTERO
        //if(lf instanceof INTEGER && rt instanceof CHAR )
        //    return Potenciar((INTEGER) lf, (CHAR) rt);
        //if(lf instanceof CHAR && rt instanceof INTEGER)
        //    return Potenciar((CHAR) lf, (INTEGER)rt);
        //if(lf instanceof BOOL && rt instanceof INTEGER)
        //    return Potenciar((BOOL) lf, (INTEGER)rt);
        //if(lf instanceof INTEGER && rt instanceof BOOL)
        //    return Potenciar((INTEGER) lf, (BOOL)rt);
        if (lf instanceof INTEGER && rt instanceof INTEGER)
            return Potenciar((INTEGER) lf, (INTEGER) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return AlgebraOp("Potenciar", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return AlgebraOp("Potenciar", (VECTOR) lf, rt);
        //endregion

        throw new Utils.SemanticException("Operacion entre tipos ( {" + lf.typo + "} ^ {" + lf.typo + "} ) no admitida");
    }

    //region METH
    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Potenciar(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(Math.pow(lf.GetValue(), rt.GetValue()));
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Potenciar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt) {
        return new NUMERIC(Math.pow(lf.GetValue(), rt.GetValue()));
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Potenciar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull CHAR rt) {
        return new NUMERIC(Math.pow(lf.GetValue(), rt.GetValue()));
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Potenciar(@NotNull CHAR lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(Math.pow(lf.GetValue(), rt.GetValue()));
    }

    @Contract("_, _ -> new")
    @NotNull
    private static PrimitiveTypoContainer.NUMERIC Potenciar(@NotNull BOOL lf, NUMERIC rt) {
        return new NUMERIC(lf.GetValueInt());
    }

    @Contract("_, _ -> new")
    @NotNull
    private static PrimitiveTypoContainer.NUMERIC Potenciar(NUMERIC lf, @NotNull BOOL rt) {
        return new NUMERIC(rt.GetValueInt());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Potenciar(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(Math.pow(lf.GetValue(), rt.GetValue()));
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Potenciar(@NotNull INTEGER lf, @NotNull CHAR rt) {
        return new INTEGER((int) Math.pow(lf.GetValue(), rt.GetValue()));
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Potenciar(@NotNull CHAR lf, @NotNull INTEGER rt) {
        return new INTEGER((int) Math.pow(lf.GetValue(), rt.GetValue()));
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Potenciar(@NotNull BOOL lf, INTEGER rt) {
        return new INTEGER(lf.GetValueInt());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Potenciar(INTEGER lf, @NotNull BOOL rt) {
        return new INTEGER(rt.GetValueInt());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static NUMERIC Potenciar(@NotNull INTEGER lf, @NotNull INTEGER rt) {
        return new NUMERIC(Math.pow(lf.GetValue(), rt.GetValue()));
    }

    //endregion
    //endregion


    //region MODULO
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Modulo(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if (rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if (lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region DOUBLE
        if (lf instanceof INTEGER && rt instanceof NUMERIC)
            return Modular((INTEGER) lf, (NUMERIC) rt);
        if (lf instanceof NUMERIC && rt instanceof INTEGER)
            return Modular((NUMERIC) lf, (INTEGER) rt);
        if (lf instanceof NUMERIC && rt instanceof NUMERIC)
            return Modular((NUMERIC) lf, (NUMERIC) rt);
        //endregion
        //region ENTERO
        if (lf instanceof INTEGER && rt instanceof INTEGER)
            return Modular((INTEGER) lf, (INTEGER) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return AlgebraOp("Modular", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return AlgebraOp("Modular", (VECTOR) lf, rt);
        //endregion

        throw new Utils.SemanticException("Operacion entre tipos ( {" + lf.typo + "} ^ {" + lf.typo + "} ) no admitida");
    }

    //region METH
    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Modular(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() % rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Modular(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt) {
        return new NUMERIC(lf.GetValue() % rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static PrimitiveTypoContainer.NUMERIC Modular(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt) {
        return new NUMERIC(lf.GetValue() % rt.GetValue());
    }

    @NotNull
    @Contract("_, _ -> new")
    private static INTEGER Modular(@NotNull INTEGER lf, @NotNull INTEGER rt) {
        return new INTEGER(lf.GetValue() % rt.GetValue());
    }

    //endregion
    //endregion


    //region ADD

    @Nullable

    public static Cntnr ADD(Cntnr lf) throws Utils.SemanticException {
        if (!(lf instanceof Reference))
            throw new Utils.SemanticException("Operacion {ref++} permitida solamente sobre referencias");


        Object val = ((Reference) lf).getValue();
        if (val instanceof INTEGER) {
            ((Reference) lf).setValue(new INTEGER(((INTEGER) val).GetValue() + 1));
            return (INTEGER) val;
        } else if (val instanceof NUMERIC) {
            ((Reference) lf).setValue(new NUMERIC(((NUMERIC) val).GetValue() + 1));
            return (NUMERIC) val;
        } else if (val instanceof CHAR) {
            ((Reference) lf).setValue(new CHAR((char) (((CHAR) val).GetValue() + 1)));
            return (CHAR) val;
        }

        throw new Utils.SemanticException("Operacion {ref++} No se puede realizar sobre variables distintas de entero, doble o char");
    }
    //endregion

    //region SUB
    public static Cntnr SUB(Cntnr lf) throws Utils.SemanticException {
        if (!(lf instanceof Reference))
            throw new Utils.SemanticException("Operacion {ref--} permitida solamente sobre referencias");


        Object val = ((Reference) lf).getValue();
        if (val instanceof INTEGER) {
            ((Reference) lf).setValue(new INTEGER(((INTEGER) val).GetValue() - 1));
            return (INTEGER) val;
        } else if (val instanceof NUMERIC) {
            ((Reference) lf).setValue(new NUMERIC(((NUMERIC) val).GetValue() - 1));
            return (NUMERIC) val;
        } else if (val instanceof CHAR) {
            ((Reference) lf).setValue(new CHAR((char) (((CHAR) val).GetValue() - 1)));
            return (CHAR) val;
        }

        throw new Utils.SemanticException("Operacion {ref--} No se puede realizar sobre variables distintas de entero, doble o char");

        //endregion
    }
    //endregion
}
