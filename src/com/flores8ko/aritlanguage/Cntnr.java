package com.flores8ko.aritlanguage;
import java.util.HashMap;
import java.util.Map;

public abstract class Cntnr {
    public String typo;
    private final Cntnr Owner;
    public final Map<String, Cntnr> _props = new HashMap<>();

    public abstract Cntnr Cast(String typo) throws Utils.SemanticException;

    public Cntnr(Cntnr owner) {
        Owner = owner;
    }

    public Cntnr(){
        Owner = null;
    }

    public String AsObjectProps() {
        StringBuilder a = new StringBuilder();
        a.append("--------------------------------\n");
        for (Map.Entry<String, Cntnr> prop : _props.entrySet()) {
            a.append(prop.getKey());
            a.append(" => ");
            a.append(prop.getValue());
            a.append("\n");
        }
        a.append("--------------------------------");
        return a.toString();
    }

    public void AddProperty(String id, Cntnr cntnr) {
        id = id.toUpperCase();
        _props.put(id, cntnr);
    }

    public Cntnr GetProperty(String id) {
        id = id.toUpperCase();
        //var val = _props.get(id);
        //if(val != null)
        //    return val;
//
        //_props.put(id, new Reference());
        //return _props.get(id);
        return _props.get(id);
    }

    public void Declare(String id, Cntnr cntnr) {
        id = id.toUpperCase();
        _props.put(id, cntnr);
//        System.out.println(id);
    }

    public String GetTypo() {
        return typo;
    }

    public void SetTypo(String typo) {
        this.typo = typo;
    }

    public Cntnr GetOwner() {
        return Owner;
    }
}
