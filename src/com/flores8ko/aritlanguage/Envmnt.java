package com.flores8ko.aritlanguage;

import com.flores8ko.aritlanguage.operations.BreakObj;
import com.flores8ko.aritlanguage.operations.ContinueObj;
import com.flores8ko.aritlanguage.operations.ReturnObj;
import com.flores8ko.aritlanguage.PrimitiveTypoContainer.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Envmnt extends Cntnr {
    public final Map<String, Object> Extra = new HashMap<>();
    private final List<Op> Operations;

    public Envmnt(Cntnr owner, List<Op> operations) {
        super(owner);
        Operations = operations;
        typo = "Ambito";
    }

    public Cntnr GO_ALL() throws Utils.ErrorCompo {
        for(Op operation : Operations){
            try {
                Object result = operation.Exe(this);
                if (result instanceof BreakObj || result instanceof ReturnObj || result instanceof ContinueObj)
                    return (Cntnr) result;
            }catch (Utils.ErrorCompo e){
                System.out.printf(e.getMessage());
            }
        }
        return new UNDEFINIED();
    }

    public String GetGraphviz(boolean descendent){
        var code = new GraphvizNode();
        code.appendLine("digraph G {");
        code.appendLine("\tgraph [splines=true overlap=false];");
        code.appendLine("\tedge[color=\"#26323A\"];");
        code.appendLine("\tgraph [bgcolor=\"#111E17\"];");
        code.appendLine("\tnode[style=filled color=\"#3f4648\" fillcolor=\"#111E17\" fontname=\"Sans\" fontcolor=\"#D76177\" ];");
        var r = new GraphvizNode("root");
        for(Op operation : Operations){
            var result = operation.GOGraphviz(this, descendent);
            r.append(result);
        }
        code.append(r);
        code.appendLine("}");
        return code.getText();
    }



    @Override
    public Cntnr Cast(String typo) throws Utils.SemanticException {
        throw new Utils.SemanticException("no se puede castear");
    }
}
