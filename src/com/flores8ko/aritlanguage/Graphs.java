package com.flores8ko.aritlanguage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Graphs {
    public static int graphNo = 1;
    private static String containers = "";
    private static String functionsCall = "";
    private static String functions = "";

    private static void addFunction(String content, int no) {
        containers += "\n<div style=\"height: 100vh\" id=\"chart" + no + "\"></div>\n";
        functionsCall += "google.charts.setOnLoadCallback(drawChart" + no + ");\n";
        functions += "function drawChart" + no + "(){\n" +
                content +
                "\n}\n\n";
    }

    private static int getGraphNo() {
        return graphNo++;
    }

    public static void addPlotGraph(
            List<Cntnr> v,
            Cntnr xlab,
            Cntnr ylab,
            Cntnr main,
            Cntnr type) {
        var no = getGraphNo();
        StringBuilder content = new StringBuilder();
        content.append(" var data = new google.visualization.DataTable();\n" +
                "        data.addColumn('number', "+xlab+");\n" +
                "        data.addColumn('number', "+ylab+");\n" +
                "\n" +
                "        data.addRows([\n");
        for (var i = 0; i < v.size(); i++) {
            var value = ((Reference) v.get(i)).getValue();
            content.append("[").append(i+1).append(", ").append(value).append("]");
            if (i == v.size() - 1)
                continue;
            content.append(",\n");
        }
        content.append("        ]);\n" +
                "\n" +
                "        var options = {\n" +
                "            chart: {\n" +
                "                title: "+main+",\n" +
                "            },\n" +
                "            width: 900,\n" +
                "            height: 500,\n" +
                "            is3D: true,\n" +
                "            series: {\n" +
                "                // Gives each series an axis name that matches the Y-axis below.\n" +
                "                0: {axis: 'Temps'},\n" +
                "            },\n" +
                "            axes: {\n" +
                "                // Adds labels to each axis; they don't have to match the axis names.\n" +
                "                y: {\n" +
                "                    Temps: {label: "+ylab+"},\n" +
                "                    Daylight: {label: 'Daylight'}\n" +
                "                }\n" +
                "            }\n" +
                "        };\n" +
                "\n" +
                "        var chart = new google.charts.Line(document.getElementById('chart" + no + "'));\n" +
                "\n" +
                "        chart.draw(data, google.charts.Line.convertOptions(options));");

        addFunction(content.toString(), no);
    }

    public static void addPlotGraph(
            List<Cntnr> v,
            Cntnr xlab,
            Cntnr ylab,
            Cntnr main,
            List<Cntnr> lims) throws Utils.SemanticException {
        var no = getGraphNo();
        StringBuilder content = new StringBuilder();

        content.append(" var data = google.visualization.arrayToDataTable([\n");
        content.append("["+xlab+", "+ylab+"],");
        var inf = ((Reference)lims.get(0)).getValue();
        var sup = ((Reference)lims.get(1)).getValue();

        for (var i = 0; i < v.size(); i++) {
            var value = ((Reference) v.get(i)).getValue();
            if(((PrimitiveTypoContainer.BOOL)Relacional.Mayor(value, sup)).GetValue())
                continue;
            if(((PrimitiveTypoContainer.BOOL)Relacional.Menor(value, inf)).GetValue())
                continue;

            content.append("[").append(i+1).append(", ").append(value).append("]");
            if (i == v.size() - 1)
                continue;
            content.append(",\n");
        }

        content.append("]);\n" +
                "var options = {\n" +
                "            title: "+main+",\n" +
                "            hAxis: {title: "+xlab+", minValue: 0, maxValue: "+v.size()+"},\n" +
                "            vAxis: {title: "+ylab+", minValue: 0, maxValue: "+sup+"},\n" +
                "            legend: 'none'\n" +
                "        };\n" +
                "\n" +
                "        var chart = new google.visualization.ScatterChart(document.getElementById('chart" + no + "'));\n" +
                "\n" +
                "        chart.draw(data, options);");


        addFunction(content.toString(), no);
    }

    public static void addPieGraph(
            List<Cntnr> x,
            List<Cntnr> labels,
            Cntnr main
    ) {
        var no = getGraphNo();
        StringBuilder content = new StringBuilder();

        content.append("var data = google.visualization.arrayToDataTable([\n");
        content.append("['Title', 'Val'],\n");
        for (var i = 0; i < x.size(); i++) {
            var title = ((Reference) labels.get(i)).getValue();
            var value = ((Reference) x.get(i)).getValue();
            content.append("[").append(title).append(", ").append(value).append("]");
            if (i == x.size() - 1)
                continue;
            content.append(",\n");
        }
        content.append("]);");

        content.append("var options = {\n" + "            title: ").append(main).append(",\n").append("            pieHole: 0.4,\n").append("        };\n");

        content.append("        var chart = new google.visualization.PieChart(document.getElementById('chart" + no + "'));\n" + "\n" + "        chart.draw(data, options);");
        addFunction(content.toString(), no);
    }

    public static void addHistGraph(
            List<Cntnr> v,
            Cntnr xlab,
            Cntnr main) {
        var no = getGraphNo();
        StringBuilder content = new StringBuilder();

        content.append("        var data = google.visualization.arrayToDataTable([\n" +
                "            [" + xlab + "],\n");
        for (var i = 0; i < v.size(); i++) {
            var value = ((Reference) v.get(i)).getValue();
            content.append("[").append(value).append("]");
            if (i == v.size() - 1)
                continue;
            content.append(",\n");
        }
        content.append("]);\n" +
                "\n" +
                "        var options = {\n" +
                "            title: " + main + ",\n" +
                "            legend: { position: 'bottom' },\n" +
                "        };\n" +
                "\n" +
                "        var chart = new google.visualization.Histogram(document.getElementById('chart" + no + "'));\n" +
                "        chart.draw(data, options);");
        addFunction(content.toString(), no);
    }

    public static void addBarGraph(
            List<Cntnr> h,
            Cntnr xlab,
            Cntnr ylab,
            Cntnr main,
            List<Cntnr> names
    ) {
        var no = getGraphNo();
        StringBuilder content = new StringBuilder();

        content.append("        var data = new google.visualization.arrayToDataTable([\n" +
                "            [" + ylab + ", " + xlab + "],\n");

        for (var i = 0; i < h.size(); i++) {
            var title = ((Reference) names.get(i)).getValue();
            var value = ((Reference) h.get(i)).getValue();
            content.append("[").append(title).append(", ").append(value).append("]");
            if (i == h.size() - 1)
                continue;
            content.append(",\n");
        }
        content.append("        ]);\n" +
                "\n" +
                "        var options = {\n" +
                "            width: 800,\n" +
                "            is3D: true,\n" +
                "            chart: {\n" +
                "                title: " + main + ",\n" +
                "            },\n" +
                "            bars: 'horizontal', // Required for Material Bar Charts.\n" +
                "            series: {\n" +
                "                0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.\n" +
                "            },\n" +
                "            axes: {\n" +
                "                x: {\n" +
                "                    distance: {label: " + xlab + "}, // Bottom x-axis.\n" +
                "                    brightness: {side: 'top', label: 'apparent magnitude'} // Top x-axis.\n" +
                "                }\n" +
                "            }\n" +
                "        };\n" +
                "\n" +
                "        var chart = new google.charts.Bar(document.getElementById('chart" + no + "'));\n" +
                "        chart.draw(data, google.charts.Bar.convertOptions(options));");

        addFunction(content.toString(), no);
    }

    public static void GenHTML() throws IOException {
        Files.write(Paths.get("files/reports/graphs.html"), getHTML().getBytes());
    }

    private static String getHTML() {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Title</title>\n" +
                "    <link rel=\"stylesheet\" href=\"css/materialize.min.css\">\n" +
                "\n" +
                "\n" +
                "</head>\n" +
                "<body>\n" +
                "<div style=\"margin-top: 2%\" class=\"row\">\n" +
                "    <div class=\"col l2 m1 s0\"></div>\n" +
                "    <div class=\"col l8 m10 s12\">" +
                containers +
                "</div>\n" +
                "    <div class=\"col l2 m1 s0\"></div>\n" +
                "</div>\n" +
                "\n" +
                "<script src=\"js/materialize.min.js\"></script>\n" +
                "<script src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
                "<script>\n" +
                "    google.charts.load('current', {'packages':['corechart', 'bar', 'line']});\n" +
                functionsCall +
                "\n\n" +
                functions +
                "</script>\n" +
                "</body>\n" +
                "</html>";
    }

    public static void restetHTML() {
        graphNo = 1;
        containers = "";
        functionsCall = "";
        functions = "";
    }
}
