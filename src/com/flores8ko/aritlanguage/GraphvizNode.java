package com.flores8ko.aritlanguage;

import java.util.ArrayList;
import java.util.List;

public class GraphvizNode {
    private final String name;
    private final String label;
    private final List<String> lines = new ArrayList<>();
    private Cntnr value;
    private String pointer;

    public GraphvizNode() {
        this.name = "";
        this.label = "l" + Utils.labelId++;
    }

    public GraphvizNode(String name, GraphvizNode... nodes) {
        this.name = name;
        this.label = "l" + Utils.labelId++;
        if (!this.label.equals("l1"))
            this.appendLine(String.format("\"%s\" [label=\"%s\"];", this.label, this.name));

        for (GraphvizNode code : nodes) {
            this.append(code);
        }
    }

    public void append(GraphvizNode graphvizNode) {
        if (graphvizNode != null) {
            if (!graphvizNode.getText().trim().equals(""))
                this.lines.addAll(graphvizNode.getLines());
            if (!this.label.equals("l1"))
                appendLine(String.format("\"%s\" -> \"%s\"", this.label, graphvizNode.label));
        }
    }

    public String getText() {
        return String.join("\n", lines);
    }

    public void appendLine(String line) {
        this.lines.add(line);
    }

    public void appendChildsConection(GraphvizNode... graphvizNode) {
        for (var n : graphvizNode) {
            append(n);
        }
    }

    public List<String> getLines() {
        return lines;
    }

    public String getName() {
        return this.name;
    }

    /*
    *
    *
    digraph G {
graph [splines=true overlap=false];
edge[color="#26323A"];
graph [bgcolor="#111E17"];
node[style=filled color="#3f4648" fillcolor="#111E17" fontname="Sans" fontcolor="#D76177" ];
  "Welcome" -> "To"
  "To" -> "Web"
  "To" -> "GraphViz!"
}

    *
    *
    * */
}
