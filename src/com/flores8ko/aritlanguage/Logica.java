package com.flores8ko.aritlanguage;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer.*;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class Logica {

    private static VECTOR LogicaOp(String Algebra, VECTOR lf, Cntnr rt) throws Utils.SemanticException {
        List<Cntnr> vals = new ArrayList<>();
        for (var val : lf.GetValueList()) {
            Cntnr v = null;
            switch (Algebra.toLowerCase()) {
                case "y":
                    v = And(val, rt);
                    break;
                case "o":
                    v = Or(val, rt);
                    break;
                case "no":
                    v = Not(val);
                    break;
            }
            vals.add(new Reference(v));
        }
        return new VECTOR(vals);
    }

    private static VECTOR LogicaOp(String Algebra, VECTOR lf, VECTOR rt) throws Utils.SemanticException {
        var lfValues = lf.GetValueList();
        var rtValues = rt.GetValueList();
        if (lfValues.size() != rtValues.size())
            throw new Utils.SemanticException("Para Operar dos vectores deben ser del mismo tamaño.");
        List<Cntnr> vals = new ArrayList<>();
        for (var i = 0; i < lf.GetValueList().size(); i++){
            Cntnr v = null;
            switch (Algebra.toLowerCase()) {
                case "y":
                    v = And(lfValues.get(i), rtValues.get(i));
                    break;
                case "o":
                    v = Or(lfValues.get(i), rtValues.get(i));
                    break;
                case "no":
                    v = Not(lfValues.get(i));
                    break;
            }
            vals.add(new Reference(v));
        }
        return new VECTOR(vals);
    }


    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr And(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if(rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if(lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region RETS
        if(lf instanceof BOOL && rt instanceof BOOL)
            return Y((BOOL) lf, (BOOL) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return LogicaOp("Y", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return LogicaOp("Y", (VECTOR) lf, rt);
        //endregion
        throw new Utils.SemanticException("Operacion entre tipos ( {"+lf.typo+"} && {"+ rt.typo+"} ) no admitida");
    }
    @NotNull
    @Contract("_, _ -> new")
    static BOOL Y(@NotNull BOOL lf, @NotNull BOOL rt){ return new BOOL(lf.GetValue() && rt.GetValue()); }



    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Or(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if(rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if(lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region RETS
        if(lf instanceof BOOL && rt instanceof BOOL)
            return O((BOOL) lf, (BOOL) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return LogicaOp("O", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return LogicaOp("O", (VECTOR) lf, rt);
        //endregion
        throw new Utils.SemanticException("Operacion entre tipos ( {"+lf.typo+"} || {"+lf.typo+"} ) no admitida");
    }
    @NotNull
    @Contract("_, _ -> new")
    static BOOL O(@NotNull BOOL lf, @NotNull BOOL rt){ return new BOOL(lf.GetValue() || rt.GetValue()); }

    @Contract("null -> fail")
    @Nullable
    public static Cntnr Not(Cntnr lf) throws Utils.SemanticException {
        if(lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region RETS
        if(lf instanceof BOOL)
            return NO((BOOL) lf);

        if (lf instanceof VECTOR)
            return LogicaOp("NO", (VECTOR) lf, null);
        //endregion
        throw new Utils.SemanticException("Operacion entre tipos (! {"+lf.typo+"} ) no admitida");
    }
    @NotNull
    @Contract("_ -> new")
    static BOOL NO(@NotNull BOOL lf){ return new BOOL(! lf.GetValue()); }
}
