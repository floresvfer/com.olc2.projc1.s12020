package com.flores8ko.aritlanguage;

public abstract class Op {
    public Object Exe(Envmnt env) throws Utils.ErrorCompo {
        try{
            return GO(env);
        }catch (Utils.SemanticException e){
            throw new Utils.ErrorCompo(e.getMessage());
        }
    }

    public abstract Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo;

    public abstract GraphvizNode GOGraphviz(Envmnt env, boolean descendent);
}
