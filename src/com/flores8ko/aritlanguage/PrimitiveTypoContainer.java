package com.flores8ko.aritlanguage;

import com.flores8ko.aritlanguage.nativeFunctions.alguno;
import com.flores8ko.aritlanguage.nativeFunctions.ascendente;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.*;

public class PrimitiveTypoContainer {
    public static class DEFAULT extends Cntnr {
        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }

        public DEFAULT() {
            typo = "DEFAULT";
        }
    }

    public static class INTEGER extends Cntnr {
        private final int value;

        public INTEGER(int value) {
            this.value = value;
            typo = "INTEGER";
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            switch (typo){
                case "NUMERIC":
                    return ToNumeric();
                case "STRING":
                    return ToString();
                default:
                    throw new Utils.SemanticException("no se puede castear");
            }
        }

        public INTEGER() {
            this.value = 0;
            typo = "INTEGER";
        }

        public NUMERIC ToNumeric() {
            return new NUMERIC(this.value);
        }

        public STRING ToString() {
            return new STRING(this.value + "");
        }

        public BOOL ToBool() {
            return this.value == 1 ? new BOOL(true) : value == 0 ? new BOOL(false) : null;
        }

        @Override
        public String toString() {
            return String.format("%d", this.value);
        }

        public int GetValue() {
            return this.value;
        }
    }

    public static class NUMERIC extends Cntnr {
        private final double value;

        public NUMERIC(double value) {
            this.value = value;
            typo = "NUMERIC";
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            if ("STRING".equals(typo)) {
                return ToString();
            }
            throw new Utils.SemanticException("no se puede castear");
        }

        public NUMERIC() {
            this.value = 0;
            typo = "NUMERIC";
        }

        public STRING ToString() {
            return new STRING(this.value + "");
        }

        @Override
        public String toString() {
            return String.format("%f", this.value);
        }

        public double GetValue() {
            return value;
        }
    }

    public static class BOOL extends Cntnr {
        private final boolean value;

        public BOOL(boolean value) {
            this.value = value;
            typo = "BOOL";
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            switch (typo){
                case "NUMERIC":
                    return ToNumeric();
                case "STRING":
                    return ToString();
                case "INTEGER":
                    return ToInteger();
                default:
                    throw new Utils.SemanticException("no se puede castear");
            }
        }

        public BOOL() {
            this.value = true;
            typo = "BOOL";
        }

        public INTEGER ToInteger() {
            return new INTEGER(this.value ? 1 : 0);
        }

        public NUMERIC ToNumeric() {
            return new NUMERIC(this.value ? 1 : 0);
        }

        public STRING ToString() {
            return new STRING(this.toString());
        }

        @Override
        public String toString() {
            return this.value ? "True" : "False";
        }

        public boolean GetValue() {
            return this.value;
        }

        public int GetValueInt() {
            return value ? 1 : 0;
        }
    }

    public static class CHAR extends Cntnr {
        private final char value;

        public CHAR(char value) {
            this.value = value;
            typo = "CHAR";
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }

        public CHAR() {
            this.value = ' ';
            typo = "CHAR";
        }

        @Override
        public String toString() {
            return String.format("%c", this.value);
        }

        public char GetValue() {
            return this.value;
        }

        public String GetStringValue() {
            return "" + this.value;
        }
    }

    public static class UNDEFINIED extends Cntnr {
        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            return Utils.DefaultValue(typo);
        }

        public UNDEFINIED() {
            typo = "UNDEFINIED";
        }

        @Override
        public String toString() {
            return "undefinied";
        }
    }

    public static class NULL extends Cntnr {
        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            return Utils.DefaultValue(typo);
        }

        public NULL() {
            typo = "NULL";
        }

        @Override
        public String toString() {
            return null;
        }

        public Object GetValue() {
            return null;
        }
    }

    public static class STRING extends Cntnr {
        private final String value;

        public STRING(String value) {
            this.value = value;
            typo = "STRING";
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }

        public STRING() {
            this.value = null;
            typo = "STRING";
        }

        @Override
        public String toString() {
            return "'" + this.value + "'";
        }

        public String GetValue() {
            return this.value;
        }
    }

    public static class ARRAY extends Cntnr {
        private final List<Cntnr> value;
        public ARRAY(List<Cntnr> value) {
            this.value = value;
            try {
                this.Declare("ascendente", new ascendente(this));
                this.Declare("alguno", new alguno(this));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }
            typo = "ARRAY";
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }

        public ARRAY() {
            this.value = new ArrayList<>();
            try {
                this.Declare("ascendente", new ascendente(this));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }
            typo = "ARRAY";
        }

        @Override
        public String toString() {
            StringBuilder log = new StringBuilder(typo + " (" + this.value.size() + ") [ ");
            for (var i = 0; i < value.size(); i++) {
                log.append(((Reference) value.get(i)).getValue());
                if (i != value.size() - 1)
                    log.append(", ");
            }
            log.append(" ]");
            return log.toString();
        }

        public Cntnr GetValue(int index) {
            try {
                return this.value.get(index);
            } catch (Exception e) {
                var size = this.value.size();
                while (size <= index) {
                    this.value.add(new Reference());
                    size++;
                }
                return this.value.get(index);
            }
        }

        public List<String> GetStringList() throws Utils.SemanticException {
            List<String> tmp = new ArrayList<>();
            for (var t : value) {
                if (t instanceof Reference)
                    t = ((Reference) t).getValue();

                if (!(t instanceof STRING))
                    throw new Utils.SemanticException("Valor (" + t.typo + ") no es de tipo cadena");

                tmp.add(((STRING) t).GetValue());
            }
            return tmp;
        }

        private boolean isStringList() {
            for (var t : value) {
                if (t instanceof Reference)
                    t = ((Reference) t).getValue();

                if (!(t instanceof STRING))
                    return false;
            }
            return true;
        }

        private boolean isIntList() {
            for (var t : value) {
                if (t instanceof Reference)
                    t = ((Reference) t).getValue();

                if (!(t instanceof INTEGER))
                    return false;
            }
            return true;
        }

        public List<Integer> GetIntegerList() throws Utils.SemanticException {
            List<Integer> tmp = new ArrayList<>();
            for (var t : value) {
                if (t instanceof Reference)
                    t = ((Reference) t).getValue();

                if (!(t instanceof INTEGER))
                    throw new Utils.SemanticException("Valor (" + t.typo + ") no es de tipo entero");

                tmp.add(((INTEGER) t).GetValue());
            }
            return tmp;
        }

        public void ascendente() throws Utils.SemanticException {
            if (!isIntList() && !isStringList())
                throw new Utils.SemanticException("Funcion ascendente, aplicable solo sobre arreglos homogeneos de tipo entero o cadena");

            if (isIntList())
                sortListIntDesc();

            if (isStringList())
                sortListStringDesc();
        }

        public void descendente() throws Utils.SemanticException {
            if (!isIntList() && !isStringList())
                throw new Utils.SemanticException("Funcion descendente, aplicable solo sobre arreglos homogeneos de tipo entero o cadena");

            if (isIntList())
                sortListIntDesc();

            if (isStringList())
                sortListStringDesc();

            Collections.reverse(this.value);
        }

        private void sortListStringDesc() {
            Cntnr tmp;
            if (value.size() > 0) {
                for (int x = 0; x < value.size(); x++) {
                    for (int i = 0; i < value.size() - x - 1; i++) {
                        var a = value.get(i);
                        var b = value.get(i + 1);
                        if (a instanceof Reference)
                            a = ((Reference) a).getValue();
                        if (b instanceof Reference)
                            b = ((Reference) b).getValue();

                        int compare = ((STRING) a).GetValue().compareTo(((STRING) b).GetValue());
                        if (compare > 0) {
                            tmp = value.get(i);
                            value.set(i, value.get(i + 1));
                            value.set(i + 1, tmp);
                        }
                    }
                }
            }
        }

        private void sortListIntDesc() {
            Cntnr tmp;
            if (value.size() > 0) {
                for (int x = 0; x < value.size(); x++) {
                    for (int i = 0; i < value.size() - x - 1; i++) {
                        var a = value.get(i);
                        var b = value.get(i + 1);
                        if (a instanceof Reference)
                            a = ((Reference) a).getValue();
                        if (b instanceof Reference)
                            b = ((Reference) b).getValue();
                        if (((INTEGER) a).GetValue() > (((INTEGER) b).GetValue())) {
                            tmp = value.get(i);
                            value.set(i, value.get(i + 1));
                            value.set(i + 1, tmp);
                        }
                    }
                }
            }
        }

        public List<Cntnr> GetValueList() {
            return this.value;
        }
    }

    public static class OBJECT extends Cntnr {
        private final Map<String, Cntnr> Atributos;

        public OBJECT(Map<String, Cntnr> atributos) {
            Atributos = atributos;
            for (var name : atributos.keySet()) {
                Cntnr value = (Cntnr) atributos.get(name);
                Reference reference = new Reference();
                reference.PutValueOnReference(value);
                this.Declare(name, reference);
            }
            typo = "OBJECT";
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }

        public OBJECT() {
            Atributos = new HashMap<>();
            typo = "OBJECT";
        }

        @Override
        public String toString() {
            StringBuilder log = new StringBuilder("Object { ");
            var i = 0;
            for (var name : this._props.keySet()) {
                String key = name.toString();
                Cntnr value = _props.get(key);

                if (value instanceof Reference)
                    value = ((Reference) value).getValue();

                log.append(key).append(": ").append(value);
                if (i != _props.keySet().size() - 1)
                    log.append(", ");
                i++;
            }
            log.append(" }");
            return log.toString();
        }

        public Cntnr GetValue(String key) {
//            var val = this.Atributos.get(key);
//            if(val != null)
//                return val;
//
//            Atributos.put(key, new Reference());
//            return this.Atributos.get(key);

            var val = this.GetProperty(key);
            if (val != null)
                return val;

            this.Declare(key, new Reference());
            return this.GetProperty(key);

        }
    }

    public static class LIST extends ARRAY {
        public LIST(List<Cntnr> value) {
            super(value);
            typo = "LIST";
        }

        public LIST() {
            typo = "LIST";
        }
    }

    public static class VECTOR extends ARRAY {
        public VECTOR(List<Cntnr> value) {
            super(value);
            typo = "VECTOR";
        }

        public VECTOR() {
            typo = "VECTOR";
        }
    }

    public static class MATRIX extends VECTOR{
        private final Cntnr rows, cols;
        public MATRIX(List<Cntnr> value, Cntnr rows, Cntnr cols){
            super(value);
            this.rows = rows;
            this.cols = cols;
            typo = "MATRIX";
        }

        public Cntnr GetValue(int row, int col){
            var myCols = ((INTEGER)cols).GetValue();

            var pos = (col - 1) + ((row - 1) * myCols);

            return GetValueList().get(pos);
        }

        public Cntnr GetNRow(){
            return this.rows;
        }

        public Cntnr GetNCol(){
            return this.cols;
        }

        @Override
        public String toString() {
            StringBuilder log = new StringBuilder();
            var myCols = ((INTEGER)cols).GetValue();
            var myRows = ((INTEGER)rows).GetValue();
            log.append("\t");
            for(var i = 1; i <= myCols; i++)
                log.append("[,").append(i).append("]\t");
            log.append("\n");
            var index = 0;
            for(var i = 1; i<= myRows ; i++){
                log.append("[").append(i).append(",]\t");
                for(var j = 0; j< myCols; j++){
                    log.append(((Reference) GetValueList().get(index)).getValue()).append("\t");
                    index++;
                }
                log.append("\n");
            }
            return log.toString();
        }
    }



    public static class VENTANA extends Cntnr {
        private final JFrame value;

        public VENTANA(String color, int width, int height) throws Utils.SemanticException {


            JFrame tmp = new JFrame();
            try {
                tmp.getContentPane().setBackground(Color.decode(color));
            } catch (NumberFormatException e) {
                throw new Utils.SemanticException("Color: " + color + " no reconocido");
            }
            tmp.setLayout(null);
            tmp.setSize(width, height);

            this.value = tmp;
            this.value.setResizable(false);
            this.value.setVisible(true);
            typo = "VENTANA";

            try {
                this.Declare("ancho", new INTEGER(width));
                this.Declare("alto", new INTEGER(height));
                this.Declare("color", new STRING(color));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }
        }

        //        public VENTANA(){
//            this.value = new JFrame();
//            typo = "VENTANA";
//        }
        @Override
        public String toString() {
            return this.AsObjectProps();
        }

        public JFrame GetValue() {
            return this.value;
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }
    }

    public static class CONTENEDOR extends Cntnr {
        private final JPanel value;

        public CONTENEDOR(String color, int width, int height, int x, int y) throws Utils.SemanticException {
            JPanel tmp = new JPanel();
            try {
                tmp.setBackground(Color.decode(color));
            } catch (NumberFormatException e) {
                throw new Utils.SemanticException("Color: " + color + " no reconocido");
            }
            tmp.setBounds(x, y, width, height);
            tmp.setBorder(null);
            tmp.setLayout(null);
            this.value = tmp;
            typo = "CONTENEDOR";
            try {
                this.Declare("ancho", new INTEGER(width));
                this.Declare("alto", new INTEGER(height));
                this.Declare("color", new STRING(color));
                this.Declare("x", new INTEGER(x));
                this.Declare("y", new INTEGER(y));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public String toString() {
            return this.value.toString();
        }

        public JPanel GetValue() {
            return this.value;
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }
    }

    public static class TEXTO extends Cntnr {
        private final JLabel value;

        public TEXTO(
                String fuente,
                int tamaño,
                String color,
                int x,
                int y,
                boolean negrita,
                boolean cursiva,
                String valor
        ) throws Utils.SemanticException {
            JLabel tmp = new JLabel(valor);
            tmp.setBackground(Color.decode("#FFFFFF"));
            try {
                tmp.setForeground(Color.decode(color));
            } catch (NumberFormatException e) {
                throw new Utils.SemanticException("Color: " + color + " no reconocido");
            }
            try {
                if (negrita && !cursiva)
                    tmp.setFont(new Font(fuente, Font.BOLD, tamaño));
                if (cursiva && !negrita)
                    tmp.setFont(new Font(fuente, Font.ITALIC, tamaño));
                if (negrita && cursiva)
                    tmp.setFont(new Font(fuente, Font.ITALIC + Font.BOLD, tamaño));
                if (!negrita && !cursiva)
                    tmp.setFont(new Font(fuente, Font.PLAIN, tamaño));
            } catch (NumberFormatException e) {
                throw new Utils.SemanticException("Fuente: " + fuente + " no reconocida");
            }
            tmp.setLayout(null);
            tmp.setLocation(x, y);
            FontMetrics fontMetrics = tmp.getFontMetrics(tmp.getFont());
            int height = fontMetrics.getHeight();
            int width = (int) (fontMetrics.stringWidth(valor) * 1.3);
            tmp.setSize(new Dimension(width, height));
            tmp.setBorder(null);
            tmp.setText(valor);
            this.value = tmp;
            typo = "TEXTO";

            try {
                this.Declare("fuente", new STRING(fuente));
                this.Declare("tamaño", new INTEGER(tamaño));
                this.Declare("color", new STRING(color));
                this.Declare("x", new INTEGER(x));
                this.Declare("y", new INTEGER(y));
                this.Declare("negrilla", new BOOL(negrita));
                this.Declare("cursiva", new BOOL(cursiva));
                this.Declare("valor", new STRING(valor));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }

        public TEXTO() {
            this.value = new JLabel();
            typo = "TEXTO";
        }

        @Override
        public String toString() {
            return this.value.toString();
        }

        public JLabel GetValue() {
            return this.value;
        }
    }

    public static class CAJATEXTO extends Cntnr {
        private final JTextField value;

        public CAJATEXTO(int alto,
                         int ancho,
                         String fuente,
                         int tamaño,
                         String color,
                         int x,
                         int y,
                         boolean negrita,
                         boolean cursiva,
                         String defecto,
                         String nombre) throws Utils.SemanticException {

            JTextField tmp = new JTextField();
            tmp.setBackground(Color.decode("#FFFFFF"));
            try {
                tmp.setForeground(Color.decode(color));
            } catch (NumberFormatException e) {
                throw new Utils.SemanticException("Color: " + color + " no reconocido");
            }
            try {
                if (negrita && !cursiva)
                    tmp.setFont(new Font(fuente, Font.BOLD, tamaño));
                if (cursiva && !negrita)
                    tmp.setFont(new Font(fuente, Font.ITALIC, tamaño));
                if (negrita && cursiva)
                    tmp.setFont(new Font(fuente, Font.ITALIC + Font.BOLD, tamaño));
                if (!negrita && !cursiva)
                    tmp.setFont(new Font(fuente, Font.PLAIN, tamaño));
            } catch (NumberFormatException e) {
                throw new Utils.SemanticException("Fuente: " + fuente + " no reconocida");
            }
            tmp.setLayout(null);
            tmp.setBounds(x, y, ancho, alto);
//            FontMetrics fontMetrics = tmp.getFontMetrics(tmp.getFont());
//            int height = fontMetrics.getHeight();
//            int width = (int) (fontMetrics.stringWidth(defecto) * 1.3);
//            tmp.setSize(new Dimension(width, height));
            tmp.setBorder(null);
            tmp.setText(defecto);

            this.value = tmp;
            this.typo = "CAJATEXTO";

            try {
                this.Declare("alto", new INTEGER(alto));
                this.Declare("ancho", new INTEGER(ancho));
                this.Declare("fuente", new STRING(fuente));
                this.Declare("tamaño", new INTEGER(tamaño));
                this.Declare("color", new STRING(color));
                this.Declare("x", new INTEGER(x));
                this.Declare("y", new INTEGER(y));
                this.Declare("negrilla", new BOOL(negrita));
                this.Declare("cursiva", new BOOL(cursiva));
                this.Declare("defecto", new STRING(defecto));
                this.Declare("nombre", new STRING(nombre));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }

        public CAJATEXTO() {
            this.value = new JTextField();
            typo = "CAJATEXTO";
        }

        @Override
        public String toString() {
            return this.value.toString();
        }

        public JTextField GetValue() {
            return this.value;
        }
    }

    public static class AREATEXTO extends Cntnr {
        private final JTextArea value;

        public AREATEXTO(int alto,
                         int ancho,
                         String fuente,
                         int tamaño,
                         String color,
                         int x,
                         int y,
                         boolean negrita,
                         boolean cursiva,
                         String defecto,
                         String nombre) throws Utils.SemanticException {

            JTextArea tmp = new JTextArea();
            tmp.setBackground(Color.decode("#FFFFFF"));
            try {
                tmp.setForeground(Color.decode(color));
            } catch (NumberFormatException e) {
                throw new Utils.SemanticException("Color: " + color + " no reconocido");
            }
            try {
                if (negrita && !cursiva)
                    tmp.setFont(new Font(fuente, Font.BOLD, tamaño));
                if (cursiva && !negrita)
                    tmp.setFont(new Font(fuente, Font.ITALIC, tamaño));
                if (negrita && cursiva)
                    tmp.setFont(new Font(fuente, Font.ITALIC + Font.BOLD, tamaño));
                if (!negrita && !cursiva)
                    tmp.setFont(new Font(fuente, Font.PLAIN, tamaño));
            } catch (NumberFormatException e) {
                throw new Utils.SemanticException("Fuente: " + fuente + " no reconocida");
            }
            tmp.setLayout(null);
            tmp.setBounds(x, y, ancho, alto);
//            FontMetrics fontMetrics = tmp.getFontMetrics(tmp.getFont());
//            int height = fontMetrics.getHeight();
//            int width = (int) (fontMetrics.stringWidth(defecto) * 1.3);
//            tmp.setSize(new Dimension(width, height));
            tmp.setBorder(null);
            tmp.setText(defecto);

            this.value = tmp;
            this.typo = "AREATEXTO";

            try {
                this.Declare("alto", new INTEGER(alto));
                this.Declare("ancho", new INTEGER(ancho));
                this.Declare("fuente", new STRING(fuente));
                this.Declare("tamaño", new INTEGER(tamaño));
                this.Declare("color", new STRING(color));
                this.Declare("x", new INTEGER(x));
                this.Declare("y", new INTEGER(y));
                this.Declare("negrilla", new BOOL(negrita));
                this.Declare("cursiva", new BOOL(cursiva));
                this.Declare("defecto", new STRING(defecto));
                this.Declare("nombre", new STRING(nombre));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }

        public AREATEXTO() {
            this.value = new JTextArea();
            typo = "AREATEXTO";
        }

        @Override
        public String toString() {
            return this.value.toString();
        }

        public JTextArea GetValue() {
            return this.value;
        }
    }

    public static class CONTROLNUMERICO extends Cntnr {
        private final JSpinner value;


        public CONTROLNUMERICO(int alto,
                               int ancho,
                               int maximo,
                               int minimo,
                               int x,
                               int y,
                               int defecto,
                               String nombre) {
            SpinnerModel m_numberSpinnerModel;
            m_numberSpinnerModel = new SpinnerNumberModel(defecto, minimo, maximo, 1);

            JSpinner tmp = new JSpinner(m_numberSpinnerModel);
            JComponent editor = new JSpinner.NumberEditor(tmp);
            tmp.setEditor(editor);

            tmp.setBounds(x, y, ancho, alto);

            this.value = tmp;
            this.typo = "CONTROLNUMERICO";

            try {
                this.Declare("alto", new INTEGER(alto));
                this.Declare("ancho", new INTEGER(ancho));
                this.Declare("maximo", new INTEGER(maximo));
                this.Declare("minimo", new INTEGER(minimo));
                this.Declare("x", new INTEGER(x));
                this.Declare("y", new INTEGER(y));
                this.Declare("defecto", new INTEGER(defecto));
                this.Declare("nombre", new STRING(nombre));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }

        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }

        public CONTROLNUMERICO() {
            this.value = new JSpinner();
            typo = "CONTROLNUMERICO";
        }

        @Override
        public String toString() {
            return this.value.toString();
        }

        public JSpinner GetValue() {
            return this.value;
        }
    }

    public static class DESPLEGABLE extends Cntnr {
        private final JComboBox value;

        public DESPLEGABLE(int alto,
                           int ancho,
                           List<String> lista,
                           int x,
                           int y,
                           String defecto,
                           String nombre) {


            JComboBox tmp = new JComboBox();
            for (var l : lista) {
                tmp.addItem(l);
            }
            tmp.setBounds(x, y, ancho, alto);
            tmp.setSelectedItem(defecto);
            this.value = tmp;
            this.typo = "DESPLEGABLE";
            try {
                this.Declare("alto", new INTEGER(alto));
                this.Declare("ancho", new INTEGER(ancho));
                List<Cntnr> strs = new ArrayList<>();
                for (var l : lista)
                    strs.add(new STRING(l));
                this.Declare("lista", new ARRAY(strs));
                this.Declare("x", new INTEGER(x));
                this.Declare("y", new INTEGER(y));
                this.Declare("defecto", new STRING(defecto));
                this.Declare("nombre", new STRING(nombre));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }

        public DESPLEGABLE() {
            this.value = new JComboBox();
            typo = "DESPLEGABLE";
        }

        @Override
        public String toString() {
            return this.value.toString();
        }

        public JComboBox GetValue() {
            return this.value;
        }
    }

    public static class BOTON extends Cntnr {
        private final JButton value;

        public BOTON(String fuente,
                     int tamaño,
                     String color,
                     int x,
                     int y,
                     String referencia,
                     String valor,
                     int alto,
                     int ancho) throws Utils.SemanticException {
            JButton tmp = new JButton(valor);
            try {
                tmp.setBackground(Color.decode(color));
            } catch (NumberFormatException e) {
                throw new Utils.SemanticException("Color: " + color + " no reconocido");
            }
            try {
                tmp.setFont(new Font(fuente, Font.PLAIN, tamaño));
            } catch (NumberFormatException e) {
                throw new Utils.SemanticException("Fuente: " + fuente + " no reconocida");
            }
            tmp.setLayout(null);
            tmp.setLocation(x, y);
            tmp.setBounds(x, y, ancho, alto);
            tmp.setBorder(null);
            this.value = tmp;
            typo = "BOTON";

            try {
                this.Declare("fuente", new STRING(fuente));
                this.Declare("tamaño", new INTEGER(tamaño));
                this.Declare("color", new STRING(color));
                this.Declare("referencia", new STRING(referencia));
                this.Declare("x", new INTEGER(x));
                this.Declare("y", new INTEGER(y));
                this.Declare("valor", new STRING(valor));
                this.Declare("alto", new INTEGER(alto));
                this.Declare("ancho", new INTEGER(ancho));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public String toString() {
            return this.value.toString();
        }

        public JButton GetValue() {
            return this.value;
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }
    }

    public static class IMAGE extends Cntnr {
        private final JImage value;

        public IMAGE(String ruta, int x, int y, int alto, int ancho) {
            JImage tmp = new JImage(ruta);
            tmp.setLayout(null);
            tmp.setBounds(x, y, alto, ancho);
            tmp.setBorder(null);
            this.value = tmp;
            this.typo = "IMAGE";

            try {
                this.Declare("ruta", new STRING(ruta));
                this.Declare("x", new INTEGER(x));
                this.Declare("y", new INTEGER(y));
                this.Declare("alto", new INTEGER(alto));
                this.Declare("ancho", new INTEGER(ancho));
            } catch (RuntimeException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public String toString() {
            return this.value.toString();
        }

        public JImage GetValue() {
            return this.value;
        }

        @Override
        public Cntnr Cast(String typo) throws Utils.SemanticException {
            throw new Utils.SemanticException("no se puede castear");
        }
    }

    private static class JImage extends JPanel {
        private BufferedImage image;

        JImage(String ruta) {
            try {
                image = ImageIO.read(new File(ruta));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), this); // see javadoc for more info on the parameters
        }
    }
}
