package com.flores8ko.aritlanguage;

import java.util.List;

public class Reference extends Cntnr {
    private Cntnr Value;

    public Reference(Cntnr value){
        typo = "Reference";
        PutValueOnReference(value);
    }

    public Reference() {
        typo = "Referencia";
        Value = new PrimitiveTypoContainer.NULL();
    }

    @Override
    public Cntnr Cast(String typo) throws Utils.SemanticException {
        var val = this.getValue();
        if (val.typo.equals(typo))
            return val;

        val = val.Cast(typo);
        this.setValue(val);
        return val;
    }

    @Override
    public String toString() {
        return String.format("[: %s :]", Value.toString());
    }

    public void PutValueOnReference(Cntnr value) {
        Cntnr v;
        if (value instanceof Reference) {
            v = ((Reference) value).Value;
        } else {
            v = value;
        }

//        if(!v.typo.equals(tipoNombre))
//            throw new RuntimeException(new Exception("Error no coinciden tipos "+v.typo+" "+tipoNombre));

        Value = v;
    }

    public Cntnr getValue() {
        return Value;
    }

    public void setValue(Cntnr value) {
        this.Value = value;
    }

    public Object Call(List<Cntnr> args) {
        for (Cntnr arg : args)
            System.out.println(arg);

        return null;
    }
}
