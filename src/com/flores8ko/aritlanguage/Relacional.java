package com.flores8ko.aritlanguage;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer.*;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class Relacional {

    private static VECTOR RelacionalOp(String Algebra, VECTOR lf, Cntnr rt) throws Utils.SemanticException {
        List<Cntnr> vals = new ArrayList<>();
        for (var val : lf.GetValueList()) {
            Cntnr v = null;
            switch (Algebra.toLowerCase()) {
                case "eq":
                    v = Igual(val, rt);
                    break;
                case "dif":
                    v = Diferente(val, rt);
                    break;
                case "may":
                    v = Mayor(val, rt);
                    break;
                case "min":
                    v = Menor(val, rt);
                    break;
                case "mayeq":
                    v = MayorIgual(val, rt);
                    break;
                case "mineq":
                    v = MenorIgual(val, rt);
                    break;

            }
            vals.add(new Reference(v));
        }
        return new VECTOR(vals);
    }

    private static VECTOR RelacionalOp(String Algebra, VECTOR lf, VECTOR rt) throws Utils.SemanticException {
        var lfValues = lf.GetValueList();
        var rtValues = rt.GetValueList();
        if (lfValues.size() != rtValues.size())
            throw new Utils.SemanticException("Para Operar dos vectores deben ser del mismo tamaño.");
        List<Cntnr> vals = new ArrayList<>();
        for (var i = 0; i < lf.GetValueList().size(); i++){
            Cntnr v = null;
            switch (Algebra.toLowerCase()) {
                case "eq":
                    v = Igual(lfValues.get(i), rtValues.get(i));
                    break;
                case "dif":
                    v = Diferente(lfValues.get(i), rtValues.get(i));
                    break;
                case "may":
                    v = Mayor(lfValues.get(i), rtValues.get(i));
                    break;
                case "min":
                    v = Menor(lfValues.get(i), rtValues.get(i));
                    break;
                case "mayeq":
                    v = MayorIgual(lfValues.get(i), rtValues.get(i));
                    break;
                case "mineq":
                    v = MenorIgual(lfValues.get(i), rtValues.get(i));
                    break;
            }
            vals.add(new Reference(v));
        }
        return new VECTOR(vals);
    }
    //region IGUAL
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Igual(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if(rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if(lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region RESTS
        if(lf instanceof INTEGER && rt instanceof INTEGER)
            return Eq((INTEGER)lf, (INTEGER)rt);
        if(lf instanceof INTEGER && rt instanceof NUMERIC)
            return Eq((INTEGER)lf, (NUMERIC)rt);
        if(lf instanceof NUMERIC && rt instanceof INTEGER)
            return Eq((NUMERIC) lf, (INTEGER) rt);
        if(lf instanceof NUMERIC && rt instanceof NUMERIC)
            return Eq((NUMERIC) lf, (NUMERIC) rt);
        //if(lf instanceof INTEGER && rt instanceof CHAR)
        //    return Eq((INTEGER) lf, (CHAR)rt);
        //if(lf instanceof CHAR && rt instanceof INTEGER)
        //    return Eq((CHAR) lf, (INTEGER)rt);
        //if(lf instanceof CHAR && rt instanceof CHAR)
        //    return Eq((CHAR) lf, (CHAR) rt);
        //if(lf instanceof BOOL && rt instanceof INTEGER)
        //    return Eq((BOOL) lf, (INTEGER)rt);
        //if(lf instanceof INTEGER && rt instanceof BOOL)
        //    return Eq((INTEGER) lf, (BOOL) rt);
        if(lf instanceof BOOL && rt instanceof BOOL)
            return Eq((BOOL) lf, (BOOL)rt);
        if(lf instanceof STRING && rt instanceof STRING)
            return Eq((STRING) lf, (STRING)rt);

        if(lf instanceof NULL && rt instanceof STRING)
            return Eq((NULL) lf, (STRING)rt);
        if(lf instanceof STRING && rt instanceof NULL)
            return Eq((STRING) lf, (NULL)rt);
        if(lf instanceof NULL && rt instanceof NULL)
            return Eq((NULL) lf, (NULL)rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return RelacionalOp("Eq", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return RelacionalOp("Eq", (VECTOR) lf, rt);
        //endregion

        throw new Utils.SemanticException("Operacion entre tipos ( {"+lf.typo+"} == {"+rt.typo+"} ) no admitida");
    }

        //region METH
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull INTEGER lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() == rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull INTEGER lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() == rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull CHAR lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() == rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull CHAR lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() == rt.GetValue());}
        //public static BOOL Eq(DOUBLE lf, CHAR rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
        //public static BOOL Eq(CHAR lf, DOUBLE rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull BOOL lf, @NotNull INTEGER rt){return new BOOL(lf.GetValueInt() == rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull INTEGER lf, @NotNull BOOL rt){return new BOOL(lf.GetValue() == rt.GetValueInt());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull BOOL lf, @NotNull BOOL rt){return new BOOL(lf.GetValueInt() == rt.GetValueInt());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull STRING lf, @NotNull STRING rt){return new BOOL(lf.GetValue().equals(rt.GetValue()));}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull STRING lf, @NotNull NULL rt){return new BOOL(lf.GetValue() == rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull NULL lf, @NotNull STRING rt){return new BOOL(lf.GetValue() == rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Eq(@NotNull NULL lf, @NotNull NULL rt){return new BOOL(lf.GetValue() == rt.GetValue());}

        //endregion
    //endregion

    //region DIFERENTE
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Diferente(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if(rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if(lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region RETS
        if(lf instanceof INTEGER && rt instanceof INTEGER)
            return Dif((INTEGER)lf, (INTEGER)rt);
        if(lf instanceof INTEGER && rt instanceof NUMERIC)
            return Dif((INTEGER)lf, (NUMERIC)rt);
        if(lf instanceof NUMERIC && rt instanceof INTEGER)
            return Dif((NUMERIC) lf, (INTEGER) rt);
        if(lf instanceof NUMERIC && rt instanceof NUMERIC)
            return Dif((NUMERIC) lf, (NUMERIC) rt);
        //if(lf instanceof INTEGER && rt instanceof CHAR)
        //    return Dif((INTEGER) lf, (CHAR)rt);
        //if(lf instanceof CHAR && rt instanceof INTEGER)
        //    return Dif((CHAR) lf, (INTEGER)rt);
        //if(lf instanceof CHAR && rt instanceof CHAR)
        //    return Dif((CHAR) lf, (CHAR) rt);
        //if(lf instanceof BOOL && rt instanceof INTEGER)
        //    return Dif((BOOL) lf, (INTEGER)rt);
        //if(lf instanceof INTEGER && rt instanceof BOOL)
        //    return Dif((INTEGER) lf, (BOOL) rt);
        if(lf instanceof BOOL && rt instanceof BOOL)
            return Dif((BOOL) lf, (BOOL)rt);
        if(lf instanceof STRING && rt instanceof STRING)
            return Dif((STRING) lf, (STRING)rt);

        if(lf instanceof NULL && rt instanceof STRING)
            return Dif((NULL) lf, (STRING)rt);
        if(lf instanceof STRING && rt instanceof NULL)
            return Dif((STRING) lf, (NULL)rt);
        if(lf instanceof NULL && rt instanceof NULL)
            return Dif((NULL) lf, (NULL)rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return RelacionalOp("Dif", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return RelacionalOp("Dif", (VECTOR) lf, rt);
        //endregion
        throw new Utils.SemanticException("Operacion entre tipos ( {"+lf.typo+"} != {"+rt.typo+"} ) no admitida");
    }
        //region METH
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull INTEGER lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() != rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) > 0.0001);}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) > 0.0001);}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) > 0.0001);}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull INTEGER lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() != rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull CHAR lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() != rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull CHAR lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() != rt.GetValue());}
        //public static BOOL Eq(DOUBLE lf, CHAR rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
        //public static BOOL Eq(CHAR lf, DOUBLE rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull BOOL lf, @NotNull INTEGER rt){return new BOOL(lf.GetValueInt() != rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull INTEGER lf, @NotNull BOOL rt){return new BOOL(lf.GetValue() != rt.GetValueInt());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull BOOL lf, @NotNull BOOL rt){return new BOOL(lf.GetValueInt() != rt.GetValueInt());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull STRING lf, @NotNull STRING rt){return new BOOL(!lf.GetValue().equals(rt.GetValue()));}

        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull NULL lf, @NotNull STRING rt){return new BOOL(lf.GetValue() != rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull STRING lf, @NotNull NULL rt){return new BOOL(lf.GetValue() != rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL Dif(@NotNull NULL lf, @NotNull NULL rt){return new BOOL(lf.GetValue() != rt.GetValue());}

    //endregion
    //endregion

    //region MIN
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Menor(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if(rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if(lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region RETS
        if(lf instanceof INTEGER && rt instanceof INTEGER)
            return Min((INTEGER)lf, (INTEGER)rt);
        if(lf instanceof INTEGER && rt instanceof NUMERIC)
            return Min((INTEGER)lf, (NUMERIC)rt);
        if(lf instanceof NUMERIC && rt instanceof INTEGER)
            return Min((NUMERIC) lf, (INTEGER) rt);
        if(lf instanceof NUMERIC && rt instanceof NUMERIC)
            return Min((NUMERIC) lf, (NUMERIC) rt);
        if(lf instanceof STRING && rt instanceof STRING)
            return Min((STRING) lf, (STRING) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return RelacionalOp("Min", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return RelacionalOp("Min", (VECTOR) lf, rt);
        //if(lf instanceof INTEGER && rt instanceof CHAR)
        //    return Min((INTEGER) lf, (CHAR)rt);
        //if(lf instanceof CHAR && rt instanceof INTEGER)
        //    return Min((CHAR) lf, (INTEGER)rt);
        //if(lf instanceof CHAR && rt instanceof CHAR)
        //    return Min((CHAR) lf, (CHAR) rt);
        //if(lf instanceof BOOL && rt instanceof INTEGER)
        //    return Min((BOOL) lf, (INTEGER)rt);
        //if(lf instanceof INTEGER && rt instanceof BOOL)
        //    return Min((INTEGER) lf, (BOOL) rt);
        //if(lf instanceof BOOL && rt instanceof BOOL)
        //    return Min((BOOL) lf, (BOOL)rt);
        //endregion
        throw new Utils.SemanticException("Operacion entre tipos ( {"+lf.typo+"} < {"+rt.typo+"} ) no admitida");
    }
    //region METH
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull INTEGER lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() < rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(lf.GetValue() < rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() < rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(lf.GetValue() < rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull PrimitiveTypoContainer.STRING lf, @NotNull PrimitiveTypoContainer.STRING rt){return new BOOL(lf.GetValue().compareTo(rt.GetValue()) < 0);}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull INTEGER lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() < rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull CHAR lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() < rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull CHAR lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() < rt.GetValue());}
    //public static BOOL Eq(DOUBLE lf, CHAR rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
    //public static BOOL Eq(CHAR lf, DOUBLE rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull BOOL lf, @NotNull INTEGER rt){return new BOOL(lf.GetValueInt() < rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull INTEGER lf, @NotNull BOOL rt){return new BOOL(lf.GetValue() < rt.GetValueInt());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL Min(@NotNull BOOL lf, @NotNull BOOL rt){return new BOOL(lf.GetValueInt() < rt.GetValueInt());}
    //endregion
    //endregion

    //region MAY
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr Mayor(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if(rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if(lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region RETS
        if(lf instanceof INTEGER && rt instanceof INTEGER)
            return May((INTEGER)lf, (INTEGER)rt);
        if(lf instanceof INTEGER && rt instanceof NUMERIC)
            return May((INTEGER)lf, (NUMERIC)rt);
        if(lf instanceof NUMERIC && rt instanceof INTEGER)
            return May((NUMERIC) lf, (INTEGER) rt);
        if(lf instanceof NUMERIC && rt instanceof NUMERIC)
            return May((NUMERIC) lf, (NUMERIC) rt);
        if(lf instanceof STRING && rt instanceof STRING)
            return May((STRING) lf, (STRING) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return RelacionalOp("May", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return RelacionalOp("May", (VECTOR) lf, rt);
        //if(lf instanceof INTEGER && rt instanceof CHAR)
        //    return May((INTEGER) lf, (CHAR)rt);
        //if(lf instanceof CHAR && rt instanceof INTEGER)
        //    return May((CHAR) lf, (INTEGER)rt);
        //if(lf instanceof CHAR && rt instanceof CHAR)
        //    return May((CHAR) lf, (CHAR) rt);
        //if(lf instanceof BOOL && rt instanceof INTEGER)
        //    return May((BOOL) lf, (INTEGER)rt);
        //if(lf instanceof INTEGER && rt instanceof BOOL)
        //    return May((INTEGER) lf, (BOOL) rt);
        //if(lf instanceof BOOL && rt instanceof BOOL)
        //    return May((BOOL) lf, (BOOL)rt);
        //endregion
        throw new Utils.SemanticException("Operacion entre tipos ( {"+lf.typo+"} > {"+rt.typo+"} ) no admitida");
    }
        //region METH
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL May(@NotNull INTEGER lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() > rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL May(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(lf.GetValue() > rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL May(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() > rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL May(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(lf.GetValue() > rt.GetValue());}
        @NotNull
        @Contract("_, _ -> new")
        private static BOOL May(@NotNull PrimitiveTypoContainer.STRING lf, @NotNull PrimitiveTypoContainer.STRING rt){return new BOOL(lf.GetValue().compareTo(rt.GetValue()) > 0);}
        //@NotNull
        //@Contract("_, _ -> new")
        //private static BOOL May(@NotNull INTEGER lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() > rt.GetValue());}
        //@NotNull
        //@Contract("_, _ -> new")
        //private static BOOL May(@NotNull CHAR lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() > rt.GetValue());}
        //@NotNull
        //@Contract("_, _ -> new")
        //private static BOOL May(@NotNull CHAR lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() > rt.GetValue());}
        //public static BOOL Eq(DOUBLE lf, CHAR rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
        //public static BOOL Eq(CHAR lf, DOUBLE rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) < 0.0001);}
        //@NotNull
        //@Contract("_, _ -> new")
        //private static BOOL May(@NotNull BOOL lf, @NotNull INTEGER rt){return new BOOL(lf.GetValueInt() > rt.GetValue());}
        //@NotNull
        //@Contract("_, _ -> new")
        //private static BOOL May(@NotNull INTEGER lf, @NotNull BOOL rt){return new BOOL(lf.GetValue() > rt.GetValueInt());}
        //@NotNull
        //@Contract("_, _ -> new")
        //private static BOOL May(@NotNull BOOL lf, @NotNull BOOL rt){return new BOOL(lf.GetValueInt() > rt.GetValueInt());}
        //endregion
    //endregion

    //region MINEQ
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr MenorIgual(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if(rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if(lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region RETS
        if(lf instanceof INTEGER && rt instanceof INTEGER)
            return MinEq((INTEGER)lf, (INTEGER)rt);
        if(lf instanceof INTEGER && rt instanceof NUMERIC)
            return MinEq((INTEGER)lf, (NUMERIC)rt);
        if(lf instanceof NUMERIC && rt instanceof INTEGER)
            return MinEq((NUMERIC) lf, (INTEGER) rt);
        if(lf instanceof NUMERIC && rt instanceof NUMERIC)
            return MinEq((NUMERIC) lf, (NUMERIC) rt);
        if(lf instanceof STRING && rt instanceof STRING)
            return MinEq((STRING) lf, (STRING) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return RelacionalOp("MinEq", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return RelacionalOp("MinEq", (VECTOR) lf, rt);
        //if(lf instanceof INTEGER && rt instanceof CHAR)
        //    return MinEq((INTEGER) lf, (CHAR)rt);
        //if(lf instanceof CHAR && rt instanceof INTEGER)
        //    return MinEq((CHAR) lf, (INTEGER)rt);
        //if(lf instanceof CHAR && rt instanceof CHAR)
        //    return MinEq((CHAR) lf, (CHAR) rt);
        //if(lf instanceof BOOL && rt instanceof INTEGER)
        //    return MinEq((BOOL) lf, (INTEGER)rt);
        //if(lf instanceof INTEGER && rt instanceof BOOL)
        //    return MinEq((INTEGER) lf, (BOOL) rt);
        //if(lf instanceof BOOL && rt instanceof BOOL)
        //    return MinEq((BOOL) lf, (BOOL)rt);
        //endregion
        throw new Utils.SemanticException("Operacion entre tipos ( {"+lf.typo+"} <= {"+rt.typo+"} ) no admitida");
    }
    //region METH
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL MinEq(@NotNull INTEGER lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() <= rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL MinEq(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(lf.GetValue() <= rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL MinEq(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() <= rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL MinEq(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(lf.GetValue() <= rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL MinEq(@NotNull PrimitiveTypoContainer.STRING lf, @NotNull PrimitiveTypoContainer.STRING rt){return new BOOL(lf.GetValue().compareTo(rt.GetValue()) <= 0);}
    //@NotNull
    //@Contract("_, _ -> new")
    //private static BOOL MinEq(@NotNull INTEGER lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() <= rt.GetValue());}
    //@NotNull
    //@Contract("_, _ -> new")
    //private static BOOL MinEq(@NotNull CHAR lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() <= rt.GetValue());}
    //@NotNull
    //@Contract("_, _ -> new")
    //private static BOOL MinEq(@NotNull CHAR lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() <= rt.GetValue());}
    ////public static BOOL Eq(DOUBLE lf, CHAR rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) <= 0.0001);}
    ////public static BOOL Eq(CHAR lf, DOUBLE rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) <= 0.0001);}
    //@NotNull
    //@Contract("_, _ -> new")
    //private static BOOL MinEq(@NotNull BOOL lf, @NotNull INTEGER rt){return new BOOL(lf.GetValueInt() <= rt.GetValue());}
    //@NotNull
    //@Contract("_, _ -> new")
    //private static BOOL MinEq(@NotNull INTEGER lf, @NotNull BOOL rt){return new BOOL(lf.GetValue() <= rt.GetValueInt());}
    //@NotNull
    //@Contract("_, _ -> new")
    //private static BOOL MinEq(@NotNull BOOL lf, @NotNull BOOL rt){return new BOOL(lf.GetValueInt() <= rt.GetValueInt());}
    //endregion
    //endregion

    //region MAYEQ
    @NotNull
    @Contract("null, null -> fail")
    public static Cntnr MayorIgual(Cntnr lf, Cntnr rt) throws Utils.SemanticException {
        if(rt instanceof Reference)
            rt = ((Reference) rt).getValue();

        if(lf instanceof Reference)
            lf = ((Reference) lf).getValue();

        //region RETS
        if(lf instanceof INTEGER && rt instanceof INTEGER)
            return MayEq((INTEGER)lf, (INTEGER)rt);
        if(lf instanceof INTEGER && rt instanceof NUMERIC)
            return MayEq((INTEGER)lf, (NUMERIC)rt);
        if(lf instanceof NUMERIC && rt instanceof INTEGER)
            return MayEq((NUMERIC) lf, (INTEGER) rt);
        if(lf instanceof NUMERIC && rt instanceof NUMERIC)
            return MayEq((NUMERIC) lf, (NUMERIC) rt);
        if(lf instanceof STRING && rt instanceof STRING)
            return MayEq((STRING) lf, (STRING) rt);

        if (lf instanceof VECTOR && rt instanceof VECTOR)
            return RelacionalOp("MayEq", (VECTOR) lf, (VECTOR) rt);

        if (lf instanceof VECTOR)
            return RelacionalOp("MayEq", (VECTOR) lf, rt);
        //if(lf instanceof INTEGER && rt instanceof CHAR)
        //    return MayEq((INTEGER) lf, (CHAR)rt);
        //if(lf instanceof CHAR && rt instanceof INTEGER)
        //    return MayEq((CHAR) lf, (INTEGER)rt);
        //if(lf instanceof CHAR && rt instanceof CHAR)
        //    return MayEq((CHAR) lf, (CHAR) rt);
        //if(lf instanceof BOOL && rt instanceof INTEGER)
        //    return MayEq((BOOL) lf, (INTEGER)rt);
        //if(lf instanceof INTEGER && rt instanceof BOOL)
        //    return MayEq((INTEGER) lf, (BOOL) rt);
        //if(lf instanceof BOOL && rt instanceof BOOL)
        //    return MayEq((BOOL) lf, (BOOL)rt);
        //endregion
        throw new Utils.SemanticException("Operacion entre tipos ( {"+lf.typo+"} >= {"+rt.typo+"} ) no admitida");
    }
    //region METH
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL MayEq(@NotNull INTEGER lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() >= rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL MayEq(@NotNull INTEGER lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(lf.GetValue() >= rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL MayEq(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() >= rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL MayEq(@NotNull PrimitiveTypoContainer.NUMERIC lf, @NotNull PrimitiveTypoContainer.NUMERIC rt){return new BOOL(lf.GetValue() >= rt.GetValue());}
    @NotNull
    @Contract("_, _ -> new")
    private static BOOL MayEq(@NotNull PrimitiveTypoContainer.STRING lf, @NotNull PrimitiveTypoContainer.STRING rt){return new BOOL(lf.GetValue().compareTo(rt.GetValue()) >= 0);}
    // @NotNull
    // @Contract("_, _ -> new")
    // private static BOOL MayEq(@NotNull INTEGER lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() >= rt.GetValue());}
    // @NotNull
    // @Contract("_, _ -> new")
    // private static BOOL MayEq(@NotNull CHAR lf, @NotNull INTEGER rt){return new BOOL(lf.GetValue() >= rt.GetValue());}
    // @NotNull
    // @Contract("_, _ -> new")
    // private static BOOL MayEq(@NotNull CHAR lf, @NotNull CHAR rt){return new BOOL(lf.GetValue() >= rt.GetValue());}
    // //public static BOOL Eq(DOUBLE lf, CHAR rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) >= 0.0001);}
    // //public static BOOL Eq(CHAR lf, DOUBLE rt){return new BOOL(Math.abs(lf.GetValue() - rt.GetValue()) >= 0.0001);}
    // @NotNull
    // @Contract("_, _ -> new")
    // private static BOOL MayEq(@NotNull BOOL lf, @NotNull INTEGER rt){return new BOOL(lf.GetValueInt() >= rt.GetValue());}
    // @NotNull
    // @Contract("_, _ -> new")
    // private static BOOL MayEq(@NotNull INTEGER lf, @NotNull BOOL rt){return new BOOL(lf.GetValue() >= rt.GetValueInt());}
    // @NotNull
    // @Contract("_, _ -> new")
    // private static BOOL MayEq(@NotNull BOOL lf, @NotNull BOOL rt){return new BOOL(lf.GetValueInt() >= rt.GetValueInt());}
    //endregion
    //endregion
}
