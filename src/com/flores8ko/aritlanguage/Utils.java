package com.flores8ko.aritlanguage;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer.BOOL;
import com.flores8ko.aritlanguage.operations.BreakObj;
import com.flores8ko.aritlanguage.operations.ContinueObj;
import com.flores8ko.parser.Parser;
import com.flores8ko.parser.T4Scanner;
import com.flores8ko.parser.javacc.Grammar;
import com.flores8ko.parser.javacc.ParseException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static int labelId = 1;

    public static Cntnr IsReference(Object val){
        Cntnr v = (Cntnr)val;
        if(v instanceof Reference)
            return ((Reference) v).getValue();
        return v;
    }

    public static String getExpressionStringValue(Op expression, Envmnt env) throws ErrorCompo, SemanticException {
        var realValue = expression.Exe(env);
        if(realValue instanceof Reference)
            realValue = ((Reference) realValue).getValue();

        if(!(realValue instanceof PrimitiveTypoContainer.STRING))
            throw new Utils.SemanticException(String.format("Tipo de argumento: %s no valido; funcion solo toma argumentos de tipo STRING.", ((Cntnr)realValue).typo));

        return  ((PrimitiveTypoContainer.STRING)realValue).GetValue();
    }

    public static double getExpressionNumericValue(Op expression, Envmnt env) throws ErrorCompo, SemanticException {
        var realValue = expression.Exe(env);
        if(realValue instanceof Reference)
            realValue = ((Reference) realValue).getValue();

        if(!(realValue instanceof PrimitiveTypoContainer.NUMERIC))
            throw new Utils.SemanticException(String.format("Tipo de argumento: %s no valido; funcion solo toma argumentos de tipo NUMERIC.", ((Cntnr)realValue).typo));

        return  ((PrimitiveTypoContainer.NUMERIC)realValue).GetValue();
    }

    static Cntnr DefaultValue(String typo) {
        switch (typo) {
            case "INTEGER":
                return new PrimitiveTypoContainer.INTEGER();
            case "BOOL":
                return new PrimitiveTypoContainer.BOOL();
            case "NUMERIC":
                return new PrimitiveTypoContainer.NUMERIC();
            default:
                return new PrimitiveTypoContainer.NULL();
        }
    }

    public static Cntnr LogicWhile(Envmnt env, Op operation, List<Op> operations, Op extra, boolean Do) throws ErrorCompo {
        Object ans = operation.Exe(env);
        if(!(ans instanceof BOOL))
            return null;

        BOOL tmp = (BOOL) ans;
        if(Do){
            Envmnt ev = new Envmnt(env, operations);

            Cntnr exAll = ev.GO_ALL();
        }

        while (tmp != null && tmp.GetValue()){
            Envmnt ev = new Envmnt(env, operations);

            Cntnr exAll = ev.GO_ALL();

            if(exAll instanceof BreakObj)
                break;
            if(exAll instanceof ContinueObj)
                continue;

            if(extra != null)
                extra.Exe(env);

            tmp = (BOOL) operation.Exe(env);

        }

        return null;
    }



    public static Cntnr FindVar(Cntnr cont, String identifier) throws SemanticException {
        Cntnr ownerOwner = cont;
//        System.out.println(cont.AsObjectProps());
        while(ownerOwner != null){
            if(ownerOwner.GetProperty(identifier) != null) {
                return ownerOwner.GetProperty(identifier);
            }

            ownerOwner = ownerOwner.GetOwner();
        }

        cont.AddProperty(identifier, new Reference());
        return cont.GetProperty(identifier);
        //throw new SemanticException("identificador {"+identifier+"} no declarado\n");
    }

    public static class SemanticException extends Exception {
        public SemanticException() { super(); }
        public SemanticException(String message) { super(message+"\n"); }
        public SemanticException(String message, Throwable cause) { super(message+"\n", cause); }
        public SemanticException(Throwable cause) { super(cause); }
    }

    public static class ErrorCompo extends Exception {
        public ErrorCompo() { super(); }
        public ErrorCompo(String message) { super(message+"\n"); }
        public ErrorCompo(String message, Throwable cause) { super(message+"\n", cause); }
        public ErrorCompo(Throwable cause) { super(cause); }
    }

    public static Cntnr ParserExpressionTest(String text) throws Exception {
        T4Scanner scanner;
        Parser parser;

        scanner = new T4Scanner(new BufferedReader(new StringReader(text)));
        parser = new Parser(scanner);

        var sentence = parser.parse().value;
        return (Cntnr) ((Op)sentence).Exe(new Envmnt(null, new ArrayList<>()));
    }

    public static void GenGraph(String data, boolean descendent){
        try {
            if(descendent){
                Files.write(Paths.get("files/graph/graphdes.dot"), data.getBytes());
            }else {
                Files.write(Paths.get("files/graph/graph.dot"), data.getBytes());
            }
//            Runtime rt = Runtime.getRuntime();
//            Process pr = rt.exec("dot -Tpng <./files/graph/graph.dot >./files/graph/graph.png");
//            pr.waitFor();



            Runtime rt = Runtime.getRuntime();
            String[] commands = {"/bin/bash", "./files/graph/gen.sh"};
            if(descendent){
                commands = new String[]{"/bin/bash", "./files/graph/gendes.sh"};
            }
            Process proc = rt.exec(commands);

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(proc.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(proc.getErrorStream()));

// Read the output from the command
            System.out.println("Here is the standard output of the command:\n");
            String s = null;
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }

// Read any errors from the attempted command
            System.out.println("Here is the standard error of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analize(String txt){
        T4Scanner scanner;
        Parser parser;

        try {
            scanner = new T4Scanner(new BufferedReader(new StringReader(txt)));
            parser = new Parser(scanner);
            Graphs.restetHTML();

            var sentences = parser.parse().value;
            var entorno = new Envmnt(null, (List<Op>) sentences);
            entorno.GO_ALL();

            System.out.println();
            System.out.println("Terminado de ejecutarse");
            GenGraph(entorno.GetGraphviz(false), false);
            Graphs.GenHTML();
        } catch (Utils.SemanticException | Utils.ErrorCompo ex) {
            System.out.println("Semantic Error");
            System.out.println(ex.getMessage());
            System.out.println();
        } catch (Exception e) {
            System.out.println("Error en proceso de compilador");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void analizejavacc(String txt){
        var analizador = new Grammar(new ByteArrayInputStream(txt.getBytes()));
        try {
            Graphs.restetHTML();
            var ops = analizador.Analizar();
            var entorno = new Envmnt(null, ops);
            entorno.GO_ALL();

            System.out.println();
            System.out.println("Terminado de ejecutarse");
            GenGraph(entorno.GetGraphviz(false), true);
            Graphs.GenHTML();
        } catch (Utils.ErrorCompo ex) {
            System.out.println("Semantic Error");
            System.out.println(ex.getMessage());
            System.out.println();
        } catch (ParseException e){
            System.out.println("Error en proceso de compilador");
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
