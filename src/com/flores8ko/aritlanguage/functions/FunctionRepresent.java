package com.flores8ko.aritlanguage.functions;


import com.flores8ko.aritlanguage.Cntnr;
import com.flores8ko.aritlanguage.Envmnt;
import com.flores8ko.aritlanguage.Utils;

import java.util.List;

public abstract class FunctionRepresent extends Cntnr {
    public abstract Cntnr EXE(Envmnt envO, List<Cntnr> args) throws Utils.ErrorCompo, Utils.SemanticException;

    @Override
    public Cntnr Cast(String typo) throws Utils.SemanticException {
        throw new Utils.SemanticException("no se puede castear");
    }
}
