package com.flores8ko.aritlanguage.functions;



import com.flores8ko.aritlanguage.*;

import java.util.ArrayList;
import java.util.List;

public class UserDefined extends FunctionRepresent {
    private final List<Op> src;
    private final List<Op> params;

    public UserDefined(List<Op> src, List<Op> params) {
        this.src = src;
        this.params = params;
    }

    public List<Op> getSrc(){
        return src;
    }

    @Override
    public Cntnr EXE(Envmnt envO, List<Cntnr> args) throws Utils.ErrorCompo {
        var env =  new Envmnt(envO, src);
        final List<Reference> references =  new ArrayList<>();
        for(Op param : params)
            references.add((Reference) param.Exe(env));

        for(int i = 0; i<args.size() && i<references.size(); i++)
            references.get(i).setValue(args.get(i));

        return env.GO_ALL();
    }
}
