package com.flores8ko.aritlanguage.nativeFunctions;

import com.flores8ko.aritlanguage.*;
import com.flores8ko.aritlanguage.functions.FunctionRepresent;
import com.flores8ko.aritlanguage.functions.Native;
import com.flores8ko.aritlanguage.operations.ReturnObj;

import java.util.ArrayList;
import java.util.List;


public class alguno extends Native {
    private final PrimitiveTypoContainer.ARRAY arreglo;

    public alguno(PrimitiveTypoContainer.ARRAY arreglo) {
        this.arreglo = arreglo;
    }

    @Override
    public Cntnr EXE(Envmnt envO, List<Cntnr> args) throws Utils.ErrorCompo, Utils.SemanticException {
        if(args.size() != 1)
            throw new Utils.SemanticException("Cantidad de parametros para funcion alguno, no debe ser mayor a 1");

        var fun = args.get(0);

        if(!(fun instanceof FunctionRepresent))
            throw new Utils.SemanticException("Se esperaba una funcion como parametro de funcion alguno");

        for(var val : this.arreglo.GetValueList()){
            if(val instanceof Reference)
                val = ((Reference) val).getValue();

            Cntnr finalVal = val;
            List<Cntnr> argsV = new ArrayList<>(){{add(finalVal);}};
            var ans = ((FunctionRepresent) fun).EXE(envO,argsV);
            if(ans instanceof ReturnObj)
                ans = ((ReturnObj) ans).GetValue();
            if(!(ans instanceof PrimitiveTypoContainer.BOOL))
                continue;
            if((((PrimitiveTypoContainer.BOOL) ans).GetValue()))
                return new ReturnObj(new PrimitiveTypoContainer.BOOL(true));
        }
        return new ReturnObj(new PrimitiveTypoContainer.BOOL(false));
    }
}
