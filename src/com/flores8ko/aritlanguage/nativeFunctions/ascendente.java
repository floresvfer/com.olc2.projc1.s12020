package com.flores8ko.aritlanguage.nativeFunctions;

import com.flores8ko.aritlanguage.Cntnr;
import com.flores8ko.aritlanguage.Envmnt;
import com.flores8ko.aritlanguage.PrimitiveTypoContainer;
import com.flores8ko.aritlanguage.Utils;
import com.flores8ko.aritlanguage.operations.ReturnObj;

import com.flores8ko.aritlanguage.functions.Native;
import java.util.List;

public class ascendente extends Native {
    private final PrimitiveTypoContainer.ARRAY arreglo;

    public ascendente(PrimitiveTypoContainer.ARRAY arreglo) {
        this.arreglo = arreglo;
    }

    @Override
    public Cntnr EXE(Envmnt envO, List<Cntnr> args) throws Utils.ErrorCompo, Utils.SemanticException {
        this.arreglo.ascendente();
        return new ReturnObj(new PrimitiveTypoContainer.UNDEFINIED());
    }

    @Override
    public Cntnr Cast(String typo) throws Utils.SemanticException { throw new Utils.SemanticException("no se puede castear"); }
}
