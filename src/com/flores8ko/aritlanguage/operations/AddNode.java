package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class AddNode extends Op {
    private Op var;

    public AddNode(Op var) {
        this.var = var;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        return Algebra.ADD((Cntnr)var.Exe(env));
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return new GraphvizNode("RE ADD", var.GOGraphviz(env, descendent), new GraphvizNode("++ (KEY sym)"));
    }
}
