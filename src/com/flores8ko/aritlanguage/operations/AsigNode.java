package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class AsigNode extends Op {
    private final Op _lf, _rt;

    public AsigNode(Op lf, Op rt) {
        _lf = lf;
        _rt = rt;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.ErrorCompo, Utils.SemanticException {
        if(_lf instanceof CreateArrVarNode)
            ((CreateArrVarNode) _lf).setFromAsign();

        Object lf = _lf.Exe(env);
        Object rt = _rt.Exe(env);

        if(!(lf instanceof Reference)) throw new Utils.ErrorCompo("No se puede asignar a {"+lf+"}, las asignaciones solo pueden ser sobre una referencia. ");

        ((Reference) lf).PutValueOnReference((Cntnr)rt);
        if(_lf instanceof CreateArrVarNode)
            ((CreateArrVarNode)_lf).FixArrayRefValues(env);
        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = _lf.GOGraphviz(env, descendent);
        var rtnode = _rt.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("= (KEY sym)");
        //gnode.appendChildsConection(lfnode);
        if(descendent)
            return new GraphvizNode("ASIG", rtnode, gnode, lfnode);
        return new GraphvizNode("ASIG", lfnode, gnode, rtnode);
    }
}
