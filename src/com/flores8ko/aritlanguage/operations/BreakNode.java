package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class BreakNode extends Op{
    @Override
    public Object GO(Envmnt env) {
        return new BreakObj();
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return new GraphvizNode("break (KEY sym)");
    }
}
