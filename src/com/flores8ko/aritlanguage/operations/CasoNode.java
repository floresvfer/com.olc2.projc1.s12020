package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

import java.util.List;

public class CasoNode {
    private Op Valor;
    private List<Op> Sentencias;

    public CasoNode(Op valor, List<Op> sentencias) {
        Valor = valor;
        Sentencias = sentencias;
    }

    public Op getValor() {
        return Valor;
    }

    public List<Op> getSentencias() {
        return Sentencias;
    }

    public GraphvizNode getGraphvizNode(Envmnt env, boolean descendent){
        var v = Valor.GOGraphviz(env, descendent);
        var b = new GraphvizNode("CASO BODY");
        if(Sentencias.size() > 0)
            for(var s : Sentencias)
                b.appendChildsConection(s.GOGraphviz(env, descendent));

        return new GraphvizNode("CASO",
                new GraphvizNode("case (KEY sym)"),
                v,
                b);
    }
}
