package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.Envmnt;
import com.flores8ko.aritlanguage.GraphvizNode;
import com.flores8ko.aritlanguage.Op;

public class ContinueNode extends Op {
    @Override
    public Object GO(Envmnt env) {
        return new ContinueObj();
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return new GraphvizNode("continue (KEY sym)");
    }
}
