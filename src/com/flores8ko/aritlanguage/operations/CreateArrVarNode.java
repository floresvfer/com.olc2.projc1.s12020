package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;
import com.flores8ko.aritlanguage.operations.functionsNodes.CNode;

import java.util.ArrayList;

public class CreateArrVarNode extends Op{
    private final Op id;
    private final Op index;
    private boolean fromAsign;
    private final boolean doubleAcces;

    public CreateArrVarNode(Op id, Op index) {
        this.id = id;
        this.index = index;
        this.fromAsign = false;
        this.doubleAcces = false;
    }

    public CreateArrVarNode(Op id, Op index, boolean doubleAcces) {
        this.id = id;
        this.index = index;
        this.fromAsign = false;
        this.doubleAcces = doubleAcces;
    }

    public void setFromAsign(){
        this.fromAsign = true;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        Cntnr ID = (Cntnr) id.Exe(env);
        if(!(ID instanceof Reference))
            throw new Utils.SemanticException("Llamada a Arreglo {"+ID+"} no definido.");

        Cntnr INDEX = (Cntnr) index.Exe(env);
        if(INDEX instanceof Reference)
            INDEX = ((Reference) INDEX).getValue();
        if(!(INDEX instanceof PrimitiveTypoContainer.INTEGER))
            throw new Utils.SemanticException("El dindice para accesar debe ser de tipo INTEGER");
        var indexValue = ((PrimitiveTypoContainer.INTEGER)INDEX).GetValue();

        if(indexValue < 1)
            throw new Utils.SemanticException("El indice para accesar debe ser mayor o igual a 1");


        var ref = ((Reference) ID).getValue();

        if(!(ref instanceof PrimitiveTypoContainer.ARRAY))
            //return new PrimitiveTypoContainer.UNDEFINIED();
            throw new Utils.SemanticException("Tipo de acceso no definido para typo: "+ref.typo.toUpperCase());

        var val = ((PrimitiveTypoContainer.ARRAY) ref).GetValue(indexValue - 1);

        if(!fromAsign && ref instanceof PrimitiveTypoContainer.LIST && !doubleAcces) {
            var a = new ArrayList<Cntnr>();
            a.add(val);
            return new Reference(new PrimitiveTypoContainer.LIST(a));
        }

        return val;
    }

    public void FixArrayRefValues(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        Cntnr ID = (Cntnr) id.Exe(env);
        var ref = ((Reference) ID).getValue();

        if(!(ref instanceof PrimitiveTypoContainer.ARRAY))
            //return new PrimitiveTypoContainer.UNDEFINIED();
            throw new Utils.SemanticException("Tipo de acceso no definido para typo: "+ref.typo.toUpperCase());

        if(ref instanceof PrimitiveTypoContainer.VECTOR) {
            var vals = ((PrimitiveTypoContainer.ARRAY) ref).GetValueList();
            CNode.Cast(vals);
        }
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
