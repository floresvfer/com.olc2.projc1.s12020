package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

import java.util.ArrayList;
import java.util.List;

public class CreateArrayNode extends Op{
    private List<Op> tmp;

    public CreateArrayNode(List<Op> tmp) { this.tmp = tmp; }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        List<Cntnr> real = new ArrayList<>();
        for(Op op : tmp) {
            Reference reference = new Reference();
            reference.PutValueOnReference((Cntnr) op.Exe(env));
            real.add(reference);
        }
        return new PrimitiveTypoContainer.ARRAY(real);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
