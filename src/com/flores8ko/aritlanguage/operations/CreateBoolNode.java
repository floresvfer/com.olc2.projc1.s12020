package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.Envmnt;
import com.flores8ko.aritlanguage.GraphvizNode;
import com.flores8ko.aritlanguage.Op;
import com.flores8ko.aritlanguage.PrimitiveTypoContainer;

public class CreateBoolNode extends Op {
    private boolean tmp;

    public CreateBoolNode(boolean tmp) {
        this.tmp = tmp;
    }

    @Override
    public Object GO(Envmnt env) {
        return new PrimitiveTypoContainer.BOOL(tmp);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var val = new GraphvizNode(this.tmp+" (BOOL)");
        return new GraphvizNode("EXPR", val);
    }
}
