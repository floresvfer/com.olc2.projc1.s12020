package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.Envmnt;
import com.flores8ko.aritlanguage.GraphvizNode;
import com.flores8ko.aritlanguage.Op;
import com.flores8ko.aritlanguage.PrimitiveTypoContainer;

public class CreateCharNode extends Op {
    private char tmp;

    public CreateCharNode(String tmp) {
        this.tmp = tmp.toCharArray()[1];
    }

    @Override
    public Object GO(Envmnt env) {
        return new PrimitiveTypoContainer.CHAR(this.tmp);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
