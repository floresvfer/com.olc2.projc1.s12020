package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.Envmnt;
import com.flores8ko.aritlanguage.GraphvizNode;
import com.flores8ko.aritlanguage.Op;
import com.flores8ko.aritlanguage.PrimitiveTypoContainer;

public class CreateDoubleNode extends Op {
    private double tmp;

    public CreateDoubleNode(double tmp) {
        this.tmp = tmp;
    }

    @Override
    public Object GO(Envmnt env) {
        return new PrimitiveTypoContainer.NUMERIC(this.tmp);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var val = new GraphvizNode(this.tmp+" (NUMERIC)");
        return new GraphvizNode("EXPR", val);
    }
}
