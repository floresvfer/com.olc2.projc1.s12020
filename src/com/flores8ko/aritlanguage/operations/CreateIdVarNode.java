package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class CreateIdVarNode extends Op {
    private String id;

    public CreateIdVarNode(String id) {
        this.id = id;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException {
        return Utils.FindVar(env, id);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var vari = new GraphvizNode(this.id+" (ID)");
        return new GraphvizNode("EXPR", vari);
    }
}
