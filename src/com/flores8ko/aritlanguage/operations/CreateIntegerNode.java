package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.Envmnt;
import com.flores8ko.aritlanguage.GraphvizNode;
import com.flores8ko.aritlanguage.Op;
import com.flores8ko.aritlanguage.PrimitiveTypoContainer;

public class CreateIntegerNode extends Op {
    private int tmp;

    public CreateIntegerNode(int tmp) {
        this.tmp = tmp;
    }

    @Override
    public Object GO(Envmnt env) {
        return new PrimitiveTypoContainer.INTEGER(tmp);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var val = new GraphvizNode(this.tmp+" (INTEGER)");
        return new GraphvizNode("EXPR", val);
    }
}
