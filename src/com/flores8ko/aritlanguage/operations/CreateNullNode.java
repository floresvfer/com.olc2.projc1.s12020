package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class CreateNullNode extends Op {

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        return new PrimitiveTypoContainer.NULL();
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var val = new GraphvizNode("null (NULL)");
        return new GraphvizNode("EXPR", val);
    }
}
