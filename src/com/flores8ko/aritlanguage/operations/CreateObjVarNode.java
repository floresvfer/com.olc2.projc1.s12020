package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class CreateObjVarNode extends Op{
    private final Op id;
    private final String attr;

    public CreateObjVarNode(Op id, String attr) {
        this.id = id;
        this.attr = attr;
    }


    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        Cntnr ID = (Cntnr) id.Exe(env);
        if(!(ID instanceof Reference))
            throw new Utils.SemanticException("Llamada a Objeto {"+ID+"} no definido");


        var ref = ((Reference) ID).getValue();

        return ref.GetProperty(attr);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
