package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

import java.util.HashMap;
import java.util.Map;

public class CreateObjectNode extends Op {
    private final Map<String, Op> attrs;

    public CreateObjectNode(Map<String, Op> attrs) {
        this.attrs = attrs;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        Map<String, Cntnr> real = new HashMap<>();
        for(var name : attrs.keySet()){
            Cntnr value = (Cntnr) attrs.get(name).Exe(env);
            Reference reference = new Reference();
            reference.PutValueOnReference(value);
            real.put(name,reference);
        }
        return new PrimitiveTypoContainer.OBJECT(real);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
