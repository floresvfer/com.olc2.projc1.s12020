package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.Envmnt;
import com.flores8ko.aritlanguage.GraphvizNode;
import com.flores8ko.aritlanguage.Op;
import com.flores8ko.aritlanguage.PrimitiveTypoContainer;

public class CreateStringNode extends Op{
    private String tmp;

    public CreateStringNode(String tmp) {
        this.tmp = tmp.replace("\"", "");
    }

    @Override
    public Object GO(Envmnt env) {
        return new PrimitiveTypoContainer.STRING(this.tmp);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var val = new GraphvizNode("'" + this.tmp +"'"+" (STRING)");
        return new GraphvizNode("EXPR", val);
    }
}
