package com.flores8ko.aritlanguage.operations;
import com.flores8ko.aritlanguage.*;
import java.util.List;

public class DeclareVarListNode extends Op {
    private final String Validator;
    private final Op Value;
    private final List<Op> DeclarationOps;

    public DeclareVarListNode(String validator, Op value, List<Op> declarationOps) {
        Validator = validator;
        Value = value;
        DeclarationOps = declarationOps;
    }

    public DeclareVarListNode(String validator, List<Op> declarationOps){

        Validator = validator;
        DeclarationOps = declarationOps;
        Value = null;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.ErrorCompo {
        env.Extra.put("validator", Validator);
        env.Extra.put("value", null);

        if(Value != null) env.Extra.put("value", Value.Exe(env));

        DeclarationOps.forEach(op -> {
            try {
                op.Exe(env);
            } catch (Utils.ErrorCompo errorCompo) {
                errorCompo.getMessage();
            }
        });
        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
