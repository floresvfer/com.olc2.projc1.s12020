package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

import org.jetbrains.annotations.NotNull;

public class DeclareVarNode extends Op {
    private String name;

    public DeclareVarNode(String name) {
        this.name = name;
    }

    @Override
    public Object GO(Envmnt env) {
        AddVarOnDeclare(env, name);
        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }

    private static void AddVarOnDeclare(@NotNull Envmnt env, String identifier){
        Cntnr value = (Cntnr) env.Extra.get("value");

        AddVarOnDeclare(env, identifier, value);
    }

    private static void AddVarOnDeclare(@NotNull Cntnr env, String identifier, Cntnr value){
        Reference reference = new Reference();
        reference.PutValueOnReference(value);
        env.Declare(identifier, reference);
    }
}
