package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class DefaultCaseNode extends Op {
    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        return new PrimitiveTypoContainer.DEFAULT();
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
