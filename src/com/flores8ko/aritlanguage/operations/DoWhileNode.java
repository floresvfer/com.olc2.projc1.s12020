package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.Envmnt;
import com.flores8ko.aritlanguage.GraphvizNode;
import com.flores8ko.aritlanguage.Op;
import com.flores8ko.aritlanguage.Utils;

import java.util.List;

public class DoWhileNode extends Op {
    private Op condicion;
    private List<Op> operations;

    public DoWhileNode(Op condicion, List<Op> operations) {
        this.condicion = condicion;
        this.operations = operations;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        Utils.LogicWhile(env, condicion, operations, null, true);
        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var cond = condicion.GOGraphviz(env, descendent);
        var b = new GraphvizNode("WHILE BODY");
        if(operations.size() > 0)
            for(var op : operations)
                b.appendChildsConection(op.GOGraphviz(env, descendent));

        return new GraphvizNode("DO WHILE",
                new GraphvizNode("do (KEY sym)"),
                b,
                new GraphvizNode("while (KEY sym)"),
                cond);
    }
}
