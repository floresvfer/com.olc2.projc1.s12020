package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

import java.util.List;

public class ForEachNode extends Op {
    private String tempVar;
    private Op element;
    private List<Op> sents;

    public ForEachNode(String tempVar, Op element, List<Op> sents) {
        this.tempVar = tempVar;
        this.element = element;
        this.sents = sents;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var element = this.element.GO(env);
        if(element instanceof Reference)
            element = ((Reference)element).getValue();

        if(!(element instanceof PrimitiveTypoContainer.ARRAY)) {
            var forEachEnvmnt = new Envmnt(env, this.sents);
            var ref = new Reference((Cntnr) element);
            forEachEnvmnt.AddProperty(this.tempVar, ref);
            forEachEnvmnt.GO_ALL();

            return null;
        }

        for (var t: ((PrimitiveTypoContainer.ARRAY)element).GetValueList()) {
            t = ((Reference)t).getValue();

            var forEachEnvmnt = new Envmnt(env, this.sents);
            var ref = new Reference(t);
            forEachEnvmnt.AddProperty(this.tempVar, ref);
            forEachEnvmnt.GO_ALL();
        }
        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var vari = new GraphvizNode(this.tempVar+" (ID)");
        var varinode = new GraphvizNode("EXPR", vari);
        var element = this.element.GOGraphviz(env, descendent);
        var b = new GraphvizNode("FOR BODY");
        if(sents.size() > 0)
            for(var sent: sents)
                b.appendChildsConection(sent.GOGraphviz(env, descendent));

        return new GraphvizNode("FOR",
                new GraphvizNode("for (KEY sym)"),
                varinode,
                element,
                b);
    }
}
