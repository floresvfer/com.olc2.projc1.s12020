package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;
import com.flores8ko.aritlanguage.functions.FunctionRepresent;

import java.util.ArrayList;
import java.util.List;

public class FunctionNode extends Op{
    private final Op id;
    private final List<Op> arguments;

    public FunctionNode(Op id, List<Op> arguments) {
        this.id = id;
        this.arguments = arguments;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        Cntnr ID = (Cntnr) id.Exe(env);
        if(ID instanceof Reference)
            ID = ((Reference) ID).getValue();



        List<Cntnr> argumentsVals = new ArrayList<>();
        for(Op arg: arguments){
            Cntnr ans = (Cntnr) arg.Exe(env);
            if(ans instanceof Reference)
                ans = ((Reference)ans).getValue();

            argumentsVals.add(ans);
        }


        if(ID instanceof FunctionRepresent) {
            var ans = ((FunctionRepresent)ID).EXE(env, argumentsVals);
            if(ans instanceof ReturnObj)
                return ((ReturnObj) ans).GetValue();
        }

        return new PrimitiveTypoContainer.UNDEFINIED();
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
