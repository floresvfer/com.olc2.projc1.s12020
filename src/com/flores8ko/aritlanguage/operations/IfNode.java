package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

import java.util.List;

public class IfNode extends Op
{
    private Op condicion;
    private List<Op> operationsTrue;
    private List<Op> operationsFalse;

    public IfNode(Op condicion, List<Op> operationsTrue, List<Op> operationsFalse) {
        this.condicion = condicion;
        this.operationsTrue = operationsTrue;
        this.operationsFalse = operationsFalse;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.ErrorCompo, Utils.SemanticException {
        Object ans = condicion.Exe(env);
        if(!(ans instanceof PrimitiveTypoContainer.BOOL))
            throw new Utils.SemanticException("condicion utilizada como parametro no soportada por si");

        PrimitiveTypoContainer.BOOL tmp = (PrimitiveTypoContainer.BOOL) ans;
        if(tmp.GetValue()){
            Envmnt EvTrue = new Envmnt(env, operationsTrue);
            return EvTrue.GO_ALL();
        }

        Envmnt EvFalse = new Envmnt(env, operationsFalse);
        return EvFalse.GO_ALL();
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var cond = condicion.GOGraphviz(env, descendent);
        var t = new GraphvizNode("IF BODY");
        var f = new GraphvizNode("IF BODY");
        for(var op : operationsTrue){
            t.appendChildsConection(op.GOGraphviz(env, descendent));
        }
        if(operationsFalse.size() == 0)
            return
                    new GraphvizNode("IF",
                            new GraphvizNode("if (KEY sym)"),
                            cond,
                            t);
        //gnode.appendChildsConection(lfnode, rtnode);
        for(var op : operationsFalse){
            f.appendChildsConection(op.GOGraphviz(env, descendent));
        }
        return
                new GraphvizNode("IF",
                        new GraphvizNode("if (KEY sym)"),
                        cond,
                        t,
                        new GraphvizNode("else (KEY sym)"),
                        f
                );
    }
}
