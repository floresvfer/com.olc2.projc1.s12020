package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class MinEqNode extends Op{
    private Op lf;
    private Op rt;

    public MinEqNode(Op lf, Op rt) {
        this.lf = lf;
        this.rt = rt;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.ErrorCompo, Utils.SemanticException {
        return Relacional.MenorIgual((Cntnr)lf.Exe(env), (Cntnr)rt.Exe(env));
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = lf.GOGraphviz(env, descendent);
        var rtnode = rt.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("<= (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        if (descendent)
            return new GraphvizNode("EXPR", rtnode, gnode, lfnode);
        return new GraphvizNode("EXPR", lfnode, gnode, rtnode);
    }
}
