package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;
import com.flores8ko.aritlanguage.functions.UserDefined;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class NewFunNode extends Op{
    private final List<Op> params;
    private final List<Op> src;

    public NewFunNode(List<Op> params, List<Op> src) {
        this.params = params;
        this.src = src;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        return AddVarOnDeclare();
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var f = new GraphvizNode("FUNCTION",
                new GraphvizNode("FUNCTION (KEY sym)"));
        var b = new GraphvizNode("FUNCTION BODY");
        var p = new GraphvizNode("FUNCTION PARAMS");
        if(params.size() > 0) {
            for (var pa : params)
                p.appendChildsConection(pa.GOGraphviz(env, descendent));
            f.appendChildsConection(p);
        }
        if(src.size() > 0) {
            for (var s : src)
                b.appendChildsConection(s.GOGraphviz(env, descendent));
            f.appendChildsConection(b);
        }
        return f;
    }

    private Cntnr AddVarOnDeclare(){
        return new UserDefined(this.src, this.params);
    }

    private Reference AddVarOnDeclare(@NotNull Cntnr env, String identifier, Cntnr value){
        Reference reference = new Reference();
        reference.PutValueOnReference(value);
        env.Declare(identifier, reference);
        return reference;
    }
}
