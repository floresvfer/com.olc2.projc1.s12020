package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;
import org.jetbrains.annotations.NotNull;

public class NewFunParamNode extends Op {
    private final String name;
    private final Op value;

    public NewFunParamNode(String name) {
        this.name = name;
        this.value = null;
    }
    public NewFunParamNode(String name, Op value){
        this.name = name;
        this.value = value;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        if(this.value == null)
            return AddVarOnDeclare(env, this.name);

        var val = this.value.Exe(env);
        if(val instanceof Reference)
            val = ((Reference)val).getValue();
        return AddVarOnDeclare(env, this.name, (Cntnr) val);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var vari = new GraphvizNode(this.name+" (ID)");
        return new GraphvizNode("EXPR", vari);
    }

    private Reference AddVarOnDeclare(@NotNull Envmnt env, String identifier, Cntnr value){
        if(value == null)
            return AddVarOnDeclare(env, identifier);
        return AddVarOnDeclarev(env, identifier, value);
    }

    private Reference AddVarOnDeclarev(@NotNull Cntnr env, String identifier, Cntnr value){
        Reference reference = new Reference(value);
        env.Declare(identifier, reference);
        return reference;
    }

    private Reference AddVarOnDeclare(@NotNull Cntnr env, String identifier){
        Reference reference = new Reference();
        env.Declare(identifier, reference);
        return reference;
    }
}
