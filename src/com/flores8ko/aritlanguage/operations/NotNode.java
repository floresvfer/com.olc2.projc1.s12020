package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class NotNode extends Op {
    private Op lf;

    public NotNode(Op lf) {
        this.lf = lf;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.ErrorCompo, Utils.SemanticException {
        return Logica.Not((Cntnr)lf.Exe(env));
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = lf.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("! (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        return new GraphvizNode("EXPR", gnode, lfnode);
    }

}
