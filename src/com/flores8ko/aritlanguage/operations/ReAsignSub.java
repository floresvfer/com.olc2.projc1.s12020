package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;
public class ReAsignSub extends Op {
    private final Op lf, rt;

    public ReAsignSub(Op lf, Op rt) {
        this.lf = lf;
        this.rt = rt;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var _lf = lf.Exe(env);
        var _rt = rt.Exe(env);

        if(!(_lf instanceof Reference)) throw new Utils.ErrorCompo("No se puede asignar a {"+lf+"}, las asignaciones solo pueden ser sobre una referencia. ");

        ((Reference)_lf).PutValueOnReference(
                Algebra.Resta(((Reference) _lf).getValue(),(Cntnr)_rt)
        );
        return ((Reference) _lf).getValue();
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
