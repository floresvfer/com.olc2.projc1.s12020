package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class ReturnNode extends Op{
    private final Op valor;

    public ReturnNode(Op valor) {
        this.valor = valor;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.ErrorCompo {
        return new ReturnObj((Cntnr)valor.Exe(env));
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var val = valor.GOGraphviz(env, descendent);
        return new GraphvizNode("RETURN",
                new GraphvizNode("return (KEY sym)"),
                val);
    }
}
