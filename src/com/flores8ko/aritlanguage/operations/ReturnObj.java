package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

public class ReturnObj extends Cntnr {
    private Cntnr returnn;

    @Override
    public Cntnr Cast(String typo) throws Utils.SemanticException {
        throw new Utils.SemanticException("no se puede castear");
    }

    public ReturnObj(Cntnr returnn) {
        this.returnn = returnn;
    }

    @Override
    public String toString() {
        return "mi objeto return (ReturnObj)";
    }

    public Cntnr GetValue(){
        return returnn;
    }

}
