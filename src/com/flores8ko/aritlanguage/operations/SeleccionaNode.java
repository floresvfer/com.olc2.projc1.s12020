package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

import java.util.List;

public class SeleccionaNode extends Op {
    private Op Condicion;
    private List<CasoNode> Casos;

    public SeleccionaNode(Op condicion, List<CasoNode> casos) {
        Condicion = condicion;
        Casos = casos;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var ans = Condicion.Exe(env);
        if (ans instanceof Reference)
            ans = ((Reference) ans).getValue();

        if (ans instanceof PrimitiveTypoContainer.INTEGER) {
            var Int = ((PrimitiveTypoContainer.INTEGER) ans);
            for (var caso : Casos)
                if (!(caso.getValor().Exe(env) instanceof PrimitiveTypoContainer.INTEGER) &&
                        !(caso.getValor().Exe(env) instanceof PrimitiveTypoContainer.DEFAULT))
                    throw new Utils.SemanticException("Los valores de los casos deben ser del mismo tipo");

            for (var caso : Casos) {
                var ev = new Envmnt(env, caso.getSentencias());
                var casoval = caso.getValor().Exe(env);
                if(casoval instanceof PrimitiveTypoContainer.DEFAULT)
                    return ev.GO_ALL();

                var ansTmp = (PrimitiveTypoContainer.INTEGER) casoval;
                if (ansTmp.GetValue() != Int.GetValue()) continue;
                return ev.GO_ALL();
            }
            return null;
        }

        if (ans instanceof PrimitiveTypoContainer.NUMERIC) {
            var Doub = ((PrimitiveTypoContainer.NUMERIC) ans);
            for (var caso : Casos)
                if (!(caso.getValor().Exe(env) instanceof PrimitiveTypoContainer.NUMERIC) &&
                        !(caso.getValor().Exe(env) instanceof PrimitiveTypoContainer.DEFAULT))
                    throw new Utils.SemanticException("Los valores de los casos deben ser del mismo tipo");

            for (var caso : Casos) {
                var ansTmp = (PrimitiveTypoContainer.NUMERIC) caso.getValor().Exe(env);
                if (Math.abs(ansTmp.GetValue() - Doub.GetValue()) > 0.00001) continue;
                var ev = new Envmnt(env, caso.getSentencias());
                return ev.GO_ALL();
            }
            return null;
        }

        //if(ans instanceof PrimitiveTypoContainer.CHAR){
        //    var Cha = ((PrimitiveTypoContainer.CHAR) ans);
        //    for (var caso : Casos)
        //        if(!(caso.getValor().Exe(env) instanceof PrimitiveTypoContainer.CHAR))
        //            throw new Utils.SemanticException("Los valores de los casos deben ser del mismo tipo");
//
        //    for(var caso: Casos){
        //        var ansTmp = (PrimitiveTypoContainer.CHAR)caso.getValor().Exe(env);
        //        if(ansTmp.GetValue() != Cha.GetValue()) continue;
        //        var ev = new Envmnt(env, caso.getSentencias());
        //        return ev.GO_ALL();
        //    }
        //}
//
        //if(ans instanceof PrimitiveTypoContainer.STRING){
        //    var Str = ((PrimitiveTypoContainer.STRING) ans);
        //    for (var caso : Casos)
        //        if(!(caso.getValor().Exe(env) instanceof PrimitiveTypoContainer.STRING))
        //            throw new Utils.SemanticException("Los valores de los casos deben ser del mismo tipo");
//
        //    for(var caso: Casos){
        //        var ansTmp = (PrimitiveTypoContainer.STRING)caso.getValor().Exe(env);
        //        if(!ansTmp.GetValue().equals(Str.GetValue())) continue;
        //        var ev = new Envmnt(env, caso.getSentencias());
        //        return ev.GO_ALL();
        //    }
        //}
//
        //if(ans instanceof PrimitiveTypoContainer.BOOL){
        //    var Doub = ((PrimitiveTypoContainer.BOOL) ans);
        //    for (var caso : Casos)
        //        if(!(caso.getValor().Exe(env) instanceof PrimitiveTypoContainer.BOOL))
        //            throw new Utils.SemanticException("Los valores de los casos deben ser del mismo tipo");
//
        //    for(var caso: Casos){
        //        var ansTmp = (PrimitiveTypoContainer.BOOL)caso.getValor().Exe(env);
        //        if(ansTmp.GetValue() != Doub.GetValue()) continue;
        //        var ev = new Envmnt(env, caso.getSentencias());
        //        return ev.GO_ALL();
        //    }
        //}

        throw new Utils.SemanticException("condicion utilizada como parametro no soportada por selecciona" + ((Cntnr)ans).typo);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var c = Condicion.GOGraphviz(env, descendent);
        var b = new GraphvizNode("SWITCH BODY");
        if(Casos.size() > 0)
        for(var cso : Casos)
            b.appendChildsConection(cso.getGraphvizNode(env, descendent));
        return
                new GraphvizNode("SWITCH",
                        new GraphvizNode("switch (KEY sym)"),
                        c,
                        b);

    }
}
