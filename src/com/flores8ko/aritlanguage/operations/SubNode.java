package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;
public class SubNode extends Op{
    private Op var;

    public SubNode(Op var) {
        this.var = var;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.ErrorCompo, Utils.SemanticException {
        return Algebra.SUB((Cntnr)var.Exe(env));
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return new GraphvizNode("RE SUB", var.GOGraphviz(env, descendent), new GraphvizNode("-- (KEY sym)"));
    }
}
