package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

import java.util.ArrayList;
import java.util.List;


public class TernarioNode extends Op{
    private Op condicion;
    private Op True;
    private Op False;

    public TernarioNode(Op condicion, Op aTrue, Op aFalse) {
        this.condicion = condicion;
        True = aTrue;
        False = aFalse;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var ans = condicion.Exe(env);
        if(ans instanceof Reference)
            ans = ((Reference) ans).getValue();

        if(!(ans instanceof PrimitiveTypoContainer.BOOL))
            throw new Utils.SemanticException("condicion utilizada com parametro no soportada por operador ternario");

        List<Op> expr = new ArrayList<>();
        if(((PrimitiveTypoContainer.BOOL) ans).GetValue())
           return True.Exe(env);

        return False.Exe(env);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var cond = condicion.GOGraphviz(env, descendent);
        var t = True.GOGraphviz(env, descendent);
        var f = False.GOGraphviz(env, descendent);
        //gnode.appendChildsConection(lfnode, rtnode);
        var strlength =
                new GraphvizNode("TERNARIO",
                        cond,
                        new GraphvizNode("? (KEY sym)"),
                        t,
                        new GraphvizNode(": (KEY sym)"),
                        f);
        return new GraphvizNode("EXPR", strlength);
    }
}
