package com.flores8ko.aritlanguage.operations;

import com.flores8ko.aritlanguage.*;

import java.util.List;

public class WhileNode extends Op {
    private Op condicion;
    List<Op> operations;

    public WhileNode(Op condicion, List<Op> operations) {
        this.condicion = condicion;
        this.operations = operations;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.ErrorCompo {
        Utils.LogicWhile(env, condicion, operations, null, false);
        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var cond = condicion.GOGraphviz(env, descendent);
        var b = new GraphvizNode("WHILE BODY");
        if(operations.size() > 0)
        for(var op : operations)
            b.appendChildsConection(op.GOGraphviz(env, descendent));

        return new GraphvizNode("WHILE",
                new GraphvizNode("while (KEY sym)"),
                cond,
                b);
    }
}
