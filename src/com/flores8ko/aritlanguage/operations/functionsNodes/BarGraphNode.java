package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class BarGraphNode extends Op {
    private Op h, xlab, ylab, main, names;

    public BarGraphNode(Op h, Op xlab, Op ylab, Op main, Op names) {
        this.h = h;
        this.xlab = xlab;
        this.ylab = ylab;
        this.main = main;
        this.names = names;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var h = this.h.Exe(env);
        var xlab = this.xlab.Exe(env);
        var ylab = this.ylab.Exe(env);
        var main = this.main.Exe(env);
        var names = this.names.Exe(env);

        h = Utils.IsReference(h);
        xlab = Utils.IsReference(xlab);
        ylab = Utils.IsReference(ylab);
        main = Utils.IsReference(main);
        names = Utils.IsReference(names);

        if(!(h instanceof PrimitiveTypoContainer.VECTOR))
            throw new Utils.SemanticException("Valor de h debe ser un vector");
        var hVal = ((PrimitiveTypoContainer.VECTOR)h);
        if(!(names instanceof PrimitiveTypoContainer.VECTOR))
            throw new Utils.SemanticException("Valor de names debe ser un vector");
        var namesVal = ((PrimitiveTypoContainer.VECTOR)names);

        if(hVal.GetValueList().size() != namesVal.GetValueList().size())
            throw new Utils.SemanticException("Tamaño de x y labels debe ser igual");

        Graphs.addBarGraph(
                hVal.GetValueList(),
                (Cntnr) xlab,
                (Cntnr) ylab,
                (Cntnr) main,
                namesVal.GetValueList()
        );
        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var h = this.h.GOGraphviz(env, descendent);
        var xlab = this.xlab.GOGraphviz(env, descendent);
        var ylab = this.ylab.GOGraphviz(env, descendent);
        var main = this.main.GOGraphviz(env, descendent);
        var names = this.names.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("bar (KEY sym)");

        return new GraphvizNode("BAR", gnode, h, xlab, ylab, main, names);
    }
}
