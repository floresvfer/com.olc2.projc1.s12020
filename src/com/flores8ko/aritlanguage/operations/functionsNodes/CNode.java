package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

import java.util.ArrayList;
import java.util.List;

public class CNode extends Op {
    private List<Op> expressions;

    public CNode(List<Op> expressions) {
        this.expressions = expressions;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        List<Cntnr> values = new ArrayList<>();

        if(expressions.size() == 1){
            Cntnr v = (Cntnr) expressions.get(0).Exe(env);
            if(v instanceof Reference)
                v = ((Reference)v).getValue();
            return v;
        }

        for(var expression : expressions){
            var value = expression.Exe(env);
            //by value
            if(value instanceof Reference)
                value = ((Reference)value).getValue();
            var realVal = new Reference();
            realVal.setValue((Cntnr) value);
            values.add(realVal);
            //by value


            //by reference
            //var realVal = value;
            //if(!(value instanceof Reference)){
            //    realVal = new Reference();
            //    ((Reference) realVal).setValue((Cntnr) value);
            //}
            //values.add((Cntnr) realVal);
            //by reference
        }
        return GetStructure(values);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = new GraphvizNode("EXPR LIST");
        for(var i = 0; i<expressions.size(); i++){
            var expr = expressions.get(i);
            if(i != 0)
                lfnode.append(new GraphvizNode("EXPR", new GraphvizNode(", (KEY sym)"), expr.GOGraphviz(env, descendent)));
            else
                lfnode.append(expr.GOGraphviz(env, descendent));
        }
        var gnode = new GraphvizNode("c (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        return new GraphvizNode("C", gnode, lfnode);
    }

    public static Cntnr GetStructure(List<Cntnr> values) throws Utils.SemanticException {
        if(SameType(values))
            return new PrimitiveTypoContainer.VECTOR(values);
        if(ContainsList(values))
            return new PrimitiveTypoContainer.LIST(LinealValues(values));
        return new PrimitiveTypoContainer.VECTOR(Cast(values));
    }

    private static boolean SameType(List<Cntnr> values){
        for(int i = 1; i<values.size(); i++){
            if(((Reference)values.get(i-1)).getValue().typo.equals("LIST") || ((Reference)values.get(i-1)).getValue().typo.equals("VECTOR"))
                return false;
            if(!
                    ((Reference) values.get(i - 1)).getValue().typo
                    .equals(((Reference)values.get(i)).getValue().typo)
            )
                return false;
        }
        return true;
    }

    private static boolean ContainsList(List<Cntnr> values){
        for(var v : values)
            if(((Reference)v).getValue().typo.equals("LIST")) return true;
        return false;
    }

    public static List<Cntnr> Cast(List<Cntnr> values) throws Utils.SemanticException {
        boolean string = false, numeric = false, integer = false;
        for(var v: values)
            switch (((Reference)v).getValue().typo){
                case "STRING":
                    string = true;
                case "NUMERIC":
                    numeric = true;
                case "INTEGER":
                    integer = true;
            }
        if(string){
            CastFullList(values, "STRING");
        }else if(numeric){
            CastFullList(values, "NUMERIC");
        }else if(integer){
            CastFullList(values, "INTEGER");
        }
        return values;
    }

    private static void CastFullList(List<Cntnr> values, String typo) throws Utils.SemanticException {
        for (Cntnr value : values) value.Cast(typo);
    }

    private static List<Cntnr> LinealValues(List<Cntnr> values){
        var newVal = new ArrayList<Cntnr>();
        for(var val: values){
            var v = ((Reference)val).getValue();
            if(!(v instanceof PrimitiveTypoContainer.LIST || v instanceof PrimitiveTypoContainer.VECTOR)){
                newVal.add(val);
                continue;
            }
            switch (v.typo){
                case "LIST":
                    assert v instanceof PrimitiveTypoContainer.LIST;
                    newVal.addAll(((PrimitiveTypoContainer.LIST)v).GetValueList());
                    break;
                case "VECTOR":
                    assert v instanceof PrimitiveTypoContainer.VECTOR;
                    newVal.addAll(((PrimitiveTypoContainer.VECTOR)v).GetValueList());
                    break;
            }
        }
        return newVal;
    }
}
