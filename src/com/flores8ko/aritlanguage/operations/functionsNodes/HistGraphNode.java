package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class HistGraphNode extends Op {
    private Op xlab, main, v;

    public HistGraphNode(Op v, Op xlab, Op main) {
        this.xlab = xlab;
        this.main = main;
        this.v = v;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var v = this.v.Exe(env);
        var xlab = this.xlab.Exe(env);
        var main = this.main.Exe(env);

        v = Utils.IsReference(v);
        xlab = Utils.IsReference(xlab);
        main = Utils.IsReference(main);

        if(!(v instanceof PrimitiveTypoContainer.VECTOR))
            throw new Utils.SemanticException("Valor de v debe ser un vector");
        var vVals = ((PrimitiveTypoContainer.VECTOR)v);

        Graphs.addHistGraph(
                vVals.GetValueList(),
                (Cntnr) xlab,
                (Cntnr) main
        );
        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var v = this.v.GOGraphviz(env, descendent);
        var xlab = this.xlab.GOGraphviz(env, descendent);
        var main = this.main.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("hist (KEY sym)");

        return new GraphvizNode("HIST", gnode, v, xlab, main);
    }
}
