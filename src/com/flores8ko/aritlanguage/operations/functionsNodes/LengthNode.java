package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class LengthNode extends Op {
    private Op expression;

    public LengthNode(Op expression) {
        this.expression = expression;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var val = expression.Exe(env);
        if (val instanceof Reference)
            val = ((Reference) val).getValue();

        if (!(
                val instanceof PrimitiveTypoContainer.ARRAY
        ))
            return new PrimitiveTypoContainer.INTEGER(1);

        return new PrimitiveTypoContainer.INTEGER(((PrimitiveTypoContainer.ARRAY)val).GetValueList().size());
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = expression.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("length (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        var fun = new GraphvizNode("LENGTH", gnode, lfnode);
        return new GraphvizNode("EXPR", fun);
    }
}
