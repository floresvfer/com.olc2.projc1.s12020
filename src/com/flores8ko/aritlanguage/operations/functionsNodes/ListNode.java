package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

import java.util.ArrayList;
import java.util.List;

public class ListNode extends Op {
    private List<Op> expressions;

    public ListNode(List<Op> expressions) {
        this.expressions = expressions;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        List<Cntnr> values = new ArrayList<>();

        for(var expression : expressions){
            var value = expression.Exe(env);
            //by value
            if(value instanceof Reference)
                value = ((Reference)value).getValue();
            var realVal = new Reference();
            realVal.setValue((Cntnr) value);
            values.add(realVal);
            //by value


            //by reference
            //var realVal = value;
            //if(!(value instanceof Reference)){
            //    realVal = new Reference();
            //    ((Reference) realVal).setValue((Cntnr) value);
            //}
            //values.add((Cntnr) realVal);
            //by reference
        }
        return new PrimitiveTypoContainer.LIST(values);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = new GraphvizNode("EXPR LIST");
        for(var i = 0; i<expressions.size(); i++){
            var expr = expressions.get(i);
            if(i != 0)
                lfnode.append(new GraphvizNode("EXPR", new GraphvizNode(", (KEY sym)"), expr.GOGraphviz(env, descendent)));
            else
                lfnode.append(expr.GOGraphviz(env, descendent));
        }
        var gnode = new GraphvizNode("list (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        return new GraphvizNode("LIST", gnode, lfnode);
    }
}
