package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

import java.util.ArrayList;

public class MatrixNode extends Op {
    private final Op val, rows, cols;

    public MatrixNode(Op val, Op rows, Op cols) {
        this.val = val;
        this.rows = rows;
        this.cols = cols;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var val = this.val.Exe(env);
        var rows = this.rows.Exe(env);
        var cols = this.cols.Exe(env);

        val = Utils.IsReference(val);
        rows = Utils.IsReference(rows);
        cols = Utils.IsReference(cols);

        if (!(rows instanceof PrimitiveTypoContainer.INTEGER))
            throw new Utils.SemanticException("Argumento nRow espera tipo INTEGER");
        if (!(cols instanceof PrimitiveTypoContainer.INTEGER))
            throw new Utils.SemanticException("Argumento nCol espera tipo INTEGER");
        if (!(val instanceof PrimitiveTypoContainer.VECTOR) &&
                !(val instanceof PrimitiveTypoContainer.NULL) &&
                !(val instanceof PrimitiveTypoContainer.NUMERIC) &&
                !(val instanceof PrimitiveTypoContainer.INTEGER) &&
                !(val instanceof PrimitiveTypoContainer.BOOL) &&
                !(val instanceof PrimitiveTypoContainer.STRING))
            throw new Utils.SemanticException("Valor para matriz no aceptado");

        var list = new ArrayList<Cntnr>();

        if(!(val instanceof PrimitiveTypoContainer.VECTOR)){
            var i = 0;
            while(i < ((PrimitiveTypoContainer.INTEGER)rows).GetValue() * ((PrimitiveTypoContainer.INTEGER)cols).GetValue()) {
                list.add(new Reference((Cntnr) val));
                i++;
            }
        }

        if(val instanceof PrimitiveTypoContainer.VECTOR){
            var i = 0;
            var vals = ((PrimitiveTypoContainer.VECTOR)val).GetValueList();
            var r = ((PrimitiveTypoContainer.INTEGER)rows).GetValue();
            var c = ((PrimitiveTypoContainer.INTEGER)cols).GetValue();

            var pos = (c - 1) + ((r - 1) * c);

            while(i < r * c ) {
                list.add(null);
                i++;
            }
            var control = 0;

            for(i = 1; i<=c; i++){
                for(var j = 1; j<=r; j++){
                    pos = (i - 1) + ((j - 1)*c);
                    list.set(pos, new Reference(((Reference)vals.get(control)).getValue()));
                    control++;
                    if(control >= vals.size())
                        control = 0;
                }
            }
        }


        return new PrimitiveTypoContainer.MATRIX(list, (Cntnr) rows, (Cntnr) cols);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
