package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

import java.util.List;

public class MeanNode extends Op {
    private Op vector;
    private Op trim;

    public MeanNode(Op vector, Op trim){
        this.vector = vector;
        this.trim = trim;
    }

    public MeanNode(Op vector) {
        this.vector = vector;
        this.trim = null;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var vector = this.vector.Exe(env);
        Cntnr trim = null;
        if(this.trim != null)
            trim = (Cntnr) this.trim.Exe(env);
        if(trim instanceof Reference)
            trim = ((Reference)trim).getValue();

        if(vector instanceof Reference)
            vector = ((Reference)vector).getValue();

        if(!(vector instanceof PrimitiveTypoContainer.VECTOR))
            throw new Utils.SemanticException("Funcion Mean solo toma como argumentos un vector y opcional un valor trim");

        var vectorVals = ((PrimitiveTypoContainer.VECTOR)vector).GetValueList();

        return GetMean(vectorVals, trim);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = vector.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("mean (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        var fun = new GraphvizNode("MEAN", gnode, lfnode);
        if(trim != null) {
            var rtnode = trim.GOGraphviz(env, descendent);
            fun.appendChildsConection(rtnode);
        }
        return new GraphvizNode("EXPR", fun);
    }

    public PrimitiveTypoContainer.NUMERIC GetMean(List<Cntnr> vals, Cntnr trim) throws Utils.SemanticException {
        float t = 0;
        float c = 0;
        if(vals.size() == 0)
            return new PrimitiveTypoContainer.NUMERIC(0);
        for(var val : vals){
            var valT = ((Reference)val).getValue();

            if(trim != null) {
                try{
                    if (((PrimitiveTypoContainer.BOOL) Relacional.Menor(valT, trim)).GetValue())
                        continue;
                }catch (Utils.SemanticException e){
                    throw new Utils.SemanticException("Valor trim no coincide con valores contenidos en vector. \n" + e.getMessage());
                }
            }

            if(valT instanceof PrimitiveTypoContainer.INTEGER) {
                t += ((PrimitiveTypoContainer.INTEGER) valT).GetValue();
            } else if(valT instanceof PrimitiveTypoContainer.NUMERIC) {
                t += ((PrimitiveTypoContainer.NUMERIC) valT).GetValue();
            } else {
                throw new Utils.SemanticException("Vector de tipo {"+ valT.typo +"} no compatible con funcion mean, solo se admiten vectores de tipo {INTEGER} o {NUMERIC}");
            }
            c++;
        }
        return new PrimitiveTypoContainer.NUMERIC(t/c);
    }
}
