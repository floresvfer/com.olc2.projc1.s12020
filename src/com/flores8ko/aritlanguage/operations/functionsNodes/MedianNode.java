package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

import java.util.ArrayList;
import java.util.List;

public class MedianNode extends Op {
    private Op vector;
    private Op trim;

    public MedianNode(Op vector, Op trim) {
        this.vector = vector;
        this.trim = trim;
    }

    public MedianNode(Op vector){
        this.vector = vector;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var vector = this.vector.Exe(env);
        Cntnr trim = null;
        if(this.trim != null)
            trim = (Cntnr) this.trim.Exe(env);
        if(trim instanceof Reference)
            trim = ((Reference)trim).getValue();

        if(vector instanceof Reference)
            vector = ((Reference)vector).getValue();

        if(!(vector instanceof PrimitiveTypoContainer.VECTOR))
            throw new Utils.SemanticException("Funcion Mean solo toma como argumentos un vector y opcional un valor trim");

        var vectorVals = ((PrimitiveTypoContainer.VECTOR)vector).GetValueList();

        return GetMedian(vectorVals, trim);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = vector.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("median (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        var fun = new GraphvizNode("MEDIAN", gnode, lfnode);
        if(trim != null) {
            var rtnode = trim.GOGraphviz(env, descendent);
            fun.appendChildsConection(rtnode);
        }
        return new GraphvizNode("EXPR", fun);
   }

    private Cntnr GetMedian(List<Cntnr> vals, Cntnr trim) throws Utils.SemanticException {
        vals = SortList(vals);
        float c = 0;
        if(vals.size() == 0)
            return new PrimitiveTypoContainer.NUMERIC(0);
        for(var val : vals){
            var valT = ((Reference)val).getValue();

            if(trim != null) {
                try{
                    if (((PrimitiveTypoContainer.BOOL) Relacional.Menor(valT, trim)).GetValue())
                        continue;
                }catch (Utils.SemanticException e){
                    throw new Utils.SemanticException("Valor trim no coincide con valores contenidos en vector. \n" + e.getMessage());
                }
            }
            c++;
        }
        if(c % 2 != 0)
            return ((Reference)vals.get((int)Math.floor(c/2))).getValue();

        var a = ((Reference)vals.get((int)Math.floor(c/2) - 1)).getValue();
        var b = ((Reference)vals.get((int)Math.floor(c/2))).getValue();
        try{
            return Algebra.Division(Algebra.Suma(a, b), new PrimitiveTypoContainer.INTEGER(2));
        }catch (Utils.SemanticException e){
            throw new Utils.SemanticException("Tamaño del vector par {"+vals.size()+"}, no se puede operar la mediana con este tipo de dato {"+a.typo+"}\n"+e.getMessage());
        }
    }

    private List<Cntnr> SortList(List<Cntnr> listOrg) throws Utils.SemanticException {
        var list = new ArrayList<>(listOrg);

        for(int i = 0; i<list.size() - 1; i++){
            for(int j = 0; j<list.size() - i - 1; j++){
                if(((PrimitiveTypoContainer.BOOL)Relacional.Mayor(
                        ((Reference)list.get(j)).getValue(),
                        ((Reference)list.get(j + 1)).getValue()
                )).GetValue()){
                    var tmp = new Reference();
                    tmp.PutValueOnReference(((Reference)list.get(j+1)).getValue());
                    list.set(j+1, list.get(j));
                    list.set(j, tmp);
                }
            }
        }
        return list;
    }
}
