package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

import java.util.HashMap;
import java.util.List;

public class ModeNode extends Op {
    private Op vector;
    private Op trim;

    public ModeNode(Op vector, Op trim) {
        this.vector = vector;
        this.trim = trim;
    }

    public ModeNode(Op vector){
        this.vector = vector;
        this.trim = null;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var vector = this.vector.Exe(env);
        Cntnr trim = null;
        if(this.trim != null)
            trim = (Cntnr) this.trim.Exe(env);
        if(trim instanceof Reference)
            trim = ((Reference)trim).getValue();

        if(vector instanceof Reference)
            vector = ((Reference)vector).getValue();

        if(!(vector instanceof PrimitiveTypoContainer.VECTOR))
            throw new Utils.SemanticException("Funcion Mean solo toma como argumentos un vector y opcional un valor trim");

        var vectorVals = ((PrimitiveTypoContainer.VECTOR)vector).GetValueList();

        return GetMode(vectorVals, trim);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = vector.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("mode (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        var fun = new GraphvizNode("MODE", gnode, lfnode);
        if(trim != null) {
            var rtnode = trim.GOGraphviz(env, descendent);
            fun.appendChildsConection(rtnode);
        }
        return new GraphvizNode("EXPR", fun);
    }

    private Cntnr GetMode(List<Cntnr> vals, Cntnr trim) throws Utils.SemanticException {
        if(vals.size() == 0)
            return new PrimitiveTypoContainer.NUMERIC(0);

        var valuesCount = new HashMap<String, Integer>();
        var realValues = new HashMap<String, Cntnr>();
        for(var v : vals){
            v = ((Reference)v).getValue();

            if(trim != null) {
                try{
                    if (((PrimitiveTypoContainer.BOOL) Relacional.Menor(v, trim)).GetValue())
                        continue;
                }catch (Utils.SemanticException e){
                    throw new Utils.SemanticException("Valor trim no coincide con valores contenidos en vector. \n" + e.getMessage());
                }
            }
            var key = v.toString();
            var val = valuesCount.get(key);
            if(val != null){
                valuesCount.replace(key, ++val);
                continue;
            }

            valuesCount.put(key, 1);
            realValues.put(key, v);
        }

        var k = "";
        var c = 0;
        for(var key : valuesCount.keySet()){
            var cT = valuesCount.get(key);
            if(cT <= c)
                continue;
            k = key;
            c = cT;
        }
        return realValues.get(k);
    }
}
