package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class NColNode extends Op {
    private final Op expr;

    public NColNode(Op expr) {
        this.expr = expr;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var expr = this.expr.Exe(env);
        if(expr instanceof Reference)
            expr = ((Reference)expr).getValue();
        if(!(expr instanceof PrimitiveTypoContainer.MATRIX))
            throw new Utils.SemanticException("Funcion solo puede tomar como argumento matrices");

        return ((PrimitiveTypoContainer.MATRIX) expr).GetNCol();
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        return null;
    }
}
