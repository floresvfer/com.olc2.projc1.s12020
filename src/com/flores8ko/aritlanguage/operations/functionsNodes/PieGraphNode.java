package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class PieGraphNode extends Op{
    private Op x;
    private Op labels;
    private Op main;

    public PieGraphNode(Op x, Op labels, Op main) {
        this.x = x;
        this.labels = labels;
        this.main = main;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var x = this.x.Exe(env);
        var labels = this.labels.Exe(env);
        var main = this.main.Exe(env);

        if(x instanceof Reference)
            x = ((Reference)x).getValue();
        if(labels instanceof Reference)
            labels = ((Reference)labels).getValue();
        if(main instanceof Reference)
            main = ((Reference)main).getValue();

        if(!(x instanceof PrimitiveTypoContainer.VECTOR))
            throw new Utils.SemanticException("Valor de x debe ser un vector");
        var xVal = ((PrimitiveTypoContainer.VECTOR)x);
        if(!(labels instanceof PrimitiveTypoContainer.VECTOR))
            throw new Utils.SemanticException("Valor de labels debe ser un vector");
        var labelsVal = ((PrimitiveTypoContainer.VECTOR)labels);

        if(xVal.GetValueList().size() != labelsVal.GetValueList().size())
            throw new Utils.SemanticException("Tamaño de x y labels debe ser igual");

        Graphs.addPieGraph(
                xVal.GetValueList(),
                labelsVal.GetValueList(),
                (Cntnr) main);
        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var x = this.x.GOGraphviz(env, descendent);
        var labels = this.x.GOGraphviz(env, descendent);
        var main = this.main.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("pie (KEY sym)");

        return new GraphvizNode("PIE", gnode, x, labels, main);
    }
}
