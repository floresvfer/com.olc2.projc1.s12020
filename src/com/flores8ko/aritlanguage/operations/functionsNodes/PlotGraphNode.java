package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class PlotGraphNode extends Op {
    private Op v, type, xlab, ylab, main;

    public PlotGraphNode(Op v, Op type, Op xlab, Op ylab, Op main) {
        this.v = v;
        this.type = type;
        this.xlab = xlab;
        this.ylab = ylab;
        this.main = main;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var v = this.v.Exe(env);
        var xlab = this.xlab.Exe(env);
        var ylab = this.ylab.Exe(env);
        var main = this.main.Exe(env);
        var type = this.type.Exe(env);

        v = Utils.IsReference(v);
        xlab = Utils.IsReference(xlab);
        ylab = Utils.IsReference(ylab);
        main = Utils.IsReference(main);
        type = Utils.IsReference(type);

        if (!(v instanceof PrimitiveTypoContainer.VECTOR))
            throw new Utils.SemanticException("Valor de v debe ser un vector");
        var vVals = ((PrimitiveTypoContainer.VECTOR) v);

        if (!(main instanceof PrimitiveTypoContainer.VECTOR)) {
            Graphs.addPlotGraph(
                    vVals.GetValueList(),
                    (Cntnr) xlab,
                    (Cntnr) ylab,
                    (Cntnr) main,
                    (Cntnr) type
            );
        } else {
            var limsVals = ((PrimitiveTypoContainer.VECTOR)main);
            Graphs.addPlotGraph(
                    vVals.GetValueList(),
                    (Cntnr) type,
                    (Cntnr) xlab,
                    (Cntnr) ylab,
                    limsVals.GetValueList()
            );
        }

        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var v = this.v.GOGraphviz(env, descendent);
        var xlab = this.xlab.GOGraphviz(env, descendent);
        var ylab = this.ylab.GOGraphviz(env, descendent);
        var main = this.main.GOGraphviz(env, descendent);
        var type = this.type.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("plot (KEY sym)");

        return new GraphvizNode("PLOT", gnode, v, xlab, ylab, main, type);
    }
}
