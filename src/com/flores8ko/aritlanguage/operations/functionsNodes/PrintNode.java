package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;
import com.flores8ko.extremeEditor;

public class PrintNode extends Op {
    private Op expresion;

    public PrintNode(Op expresion) {
        this.expresion = expresion;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.ErrorCompo {
        Object realVal = expresion.Exe(env);
        if(realVal instanceof Reference)
            realVal = ((Reference) realVal).getValue();

        extremeEditor.AddToConsole(">_ "+realVal+"\n");
        System.out.println(">_ "+realVal);
        return null;
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = expresion.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("print (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        return new GraphvizNode("PRINT", gnode, lfnode);
    }
}
