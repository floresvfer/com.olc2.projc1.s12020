package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class RoundNode extends Op {
    private Op expression;

    public RoundNode(Op expression) {
        this.expression = expression;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var val = Utils.getExpressionNumericValue(expression, env);
        return new PrimitiveTypoContainer.INTEGER((int) Math.round(val));
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = expression.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("round (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        var strlength = new GraphvizNode("ROUND", gnode, lfnode);
        return new GraphvizNode("EXPR", strlength);
    }
}
