package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class StringLenghtNode extends Op {
    private Op expression;

    public StringLenghtNode(Op expression) {
        this.expression = expression;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var val = Utils.getExpressionStringValue(expression, env);
        return new PrimitiveTypoContainer.INTEGER(val.length());
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = expression.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("stringlength (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        var strlength = new GraphvizNode("STRINGLENGTH", gnode, lfnode);
        return new GraphvizNode("EXPR", strlength);
    }
}
