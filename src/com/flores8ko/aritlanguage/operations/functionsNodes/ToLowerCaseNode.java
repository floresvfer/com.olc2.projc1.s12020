package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class ToLowerCaseNode extends Op {
    private Op expression;

    public ToLowerCaseNode(Op expression) {
        this.expression = expression;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var val = Utils.getExpressionStringValue(expression, env);
        return new PrimitiveTypoContainer.STRING(val.toLowerCase());
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = expression.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("tolowercase (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        var strlength = new GraphvizNode("TOLOWERCASE", gnode, lfnode);
        return new GraphvizNode("EXPR", strlength);
    }
}
