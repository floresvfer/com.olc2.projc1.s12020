package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class TrunkNode extends Op {
    private Op expression;

    public TrunkNode(Op expression) {
        this.expression = expression;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var val = Utils.getExpressionNumericValue(expression, env);
        return new PrimitiveTypoContainer.INTEGER((int) Math.floor(val));
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = expression.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("trunk (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        var strlength = new GraphvizNode("TRUNK", gnode, lfnode);
        return new GraphvizNode("EXPR", strlength);
    }
}
