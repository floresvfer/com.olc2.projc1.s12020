package com.flores8ko.aritlanguage.operations.functionsNodes;

import com.flores8ko.aritlanguage.*;

public class TypeOfNode extends Op {
    private Op expression;

    public TypeOfNode(Op expression) {
        this.expression = expression;
    }

    @Override
    public Object GO(Envmnt env) throws Utils.SemanticException, Utils.ErrorCompo {
        var val = expression.Exe(env);
        if(val instanceof Reference)
             val = ((Reference) val).getValue();

        return new PrimitiveTypoContainer.STRING(((Cntnr)val).typo);
    }

    @Override
    public GraphvizNode GOGraphviz(Envmnt env, boolean descendent) {
        var lfnode = expression.GOGraphviz(env, descendent);
        var gnode = new GraphvizNode("typeof (KEY sym)");
        //gnode.appendChildsConection(lfnode, rtnode);
        var strlength = new GraphvizNode("TYPEOF", gnode, lfnode);
        return new GraphvizNode("EXPR", strlength);
    }
}
