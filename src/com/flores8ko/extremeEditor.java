package com.flores8ko;

import com.flores8ko.aritlanguage.Utils;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class extremeEditor extends JFrame implements ActionListener {
    static JTextArea console;
    JMenuBar menu;
    JMenu
            archivo,
            abrir,
            nuevaPestaña,
            pestañas,
            errores;
    JMenuItem
            gxml, fs, guardar, guardarComo,
            newGxml, newFs, cerrarPesaña,
            erroresLexicos, erroresSemanticos, erroresSintacticos;
    JPanel
            upperPanel,
            lowerPanel,
            INDEX;
    JButton
            startBtn,
            startDescBtn;
    JTabbedPane
            tabs;

    private extremeEditor() throws IOException {
        console = new JTextArea();
        setLayout(null);
        setMenu();
        setStartBtn();
        setTabControl(1);
        setTitle("EXtreme Editor");
        setSize(1100, 720);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void AddToConsole(String txt) {
        console.append(txt);
    }

    static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
            new extremeEditor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JScrollPane newTab(String content, int syntax) {
        RSyntaxTextArea textArea = new RSyntaxTextArea(34, 128);
        textArea.setBounds(5, 5, 900, 700);
        if (syntax == 1)
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
        if (syntax == 0)
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_XML);
        textArea.setCodeFoldingEnabled(true);
        textArea.setText(content);
        var r = new RTextScrollPane(textArea);
        r.setBounds(5, 5, 900, 700);
        return r;
    }

    private void setConsole() {
        console = new JTextArea();
        console.disable();
        var r = new JScrollPane(console);
        r.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        r.setPreferredSize(new Dimension(1076, 225));
        r.setBounds(8, 440, 1076, 225);
        add(r);
    }

    private void setMenu() {
        menu = new JMenuBar();
        setJMenuBar(menu);

        //
        archivo = new JMenu("Archivo");
        pestañas = new JMenu("Pestañas");
        errores = new JMenu("Errores");
        menu.add(archivo);
        menu.add(pestañas);
        menu.add(errores);

        //
        abrir = new JMenu("Abrir");
        gxml = new JMenuItem("GXML");
        fs = new JMenuItem("FS");
        guardar = new JMenuItem("Guardar");
        guardarComo = new JMenuItem("Guardar Como");
//        abrir.addActionListener(this);
        gxml.addActionListener(this);
        fs.addActionListener(this);
        guardar.addActionListener(this);
        guardarComo.addActionListener(this);
        abrir.add(gxml);
        abrir.add(fs);
        archivo.add(abrir);
        archivo.add(guardar);
        archivo.add(guardarComo);

        //
        nuevaPestaña = new JMenu("Nueva Pestaña");
        newGxml = new JMenuItem("GXML");
        newFs = new JMenuItem("FS");
        cerrarPesaña = new JMenuItem("Cerrar Pestaña");
//        nuevaPestaña.addActionListener(this);
        newGxml.addActionListener(this);
        newFs.addActionListener(this);
        cerrarPesaña.addActionListener(this);
        nuevaPestaña.add(newGxml);
        nuevaPestaña.add(newFs);
        pestañas.add(nuevaPestaña);
        pestañas.add(cerrarPesaña);

        //
        erroresLexicos = new JMenuItem("Errores Lexicos");
        erroresSemanticos = new JMenuItem("Errores Semanticos");
        erroresSintacticos = new JMenuItem("Errores Sintacticos");
        erroresLexicos.addActionListener(this);
        erroresSintacticos.addActionListener(this);
        erroresSemanticos.addActionListener(this);
        errores.add(erroresLexicos);
        errores.add(erroresSemanticos);
        errores.add(erroresSintacticos);
    }

    private void setStartBtn() {
        upperPanel = new JPanel();

        startDescBtn = new JButton("START DESC");
        startBtn = new JButton("START ASC");
        startBtn.setBounds(50, 520, 80, 40);
        startDescBtn.setBounds(50, 400, 80,40);
        startDescBtn.addActionListener(this);
        startBtn.addActionListener(this);
        upperPanel.add(startBtn);
        upperPanel.add(startDescBtn);

        upperPanel.setSize(1080, 40);
        upperPanel.setBackground(Color.DARK_GRAY);
        upperPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY));

        add(upperPanel);
    }

    private void setTabControl(int syntax) throws IOException {
        lowerPanel = new JPanel();


        lowerPanel.setBounds(5, 50, 1080, 700);


        tabs = new JTabbedPane();
        tabs.setBounds(20, 0, 1080, 700);
        String contenido;
        try {
            if (syntax == 1) {
                contenido = readFile("C:\\sources\\OLC2\\test.fs", StandardCharsets.UTF_8);
            } else {
                contenido = readFile("C:\\sources\\OLC2\\test.gxml", StandardCharsets.UTF_8);
            }

        } catch (Exception e) {
            contenido = readFile("./files/tests.txt", StandardCharsets.UTF_8);
        }
        tabs.addTab("New File", newTab(contenido, syntax));
        lowerPanel.add(tabs);
        //lowerPanel.add(sp);
        add(lowerPanel);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == fs) {
            try {
                openFile(1);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        if (e.getSource() == gxml) {
            try {
                openFile(0);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        if (e.getSource() == guardar)
            saveFile();
        if (e.getSource() == guardarComo)
            saveFileAS();

        if (e.getSource() == newGxml)
            newTab(0);

        if (e.getSource() == newFs)
            newTab(1);

        if (e.getSource() == cerrarPesaña)
            closeTab();

        if (e.getSource() == erroresLexicos)
            lexErrors();
        if (e.getSource() == erroresSemanticos)
            semErrors();
        if (e.getSource() == erroresSintacticos)
            synErrors();

        if (e.getSource() == startBtn)
            analizeasc();
        if(e.getSource() == startDescBtn)
            analizedesc();
    }

    private void analizeasc() {
        console.setText("");
        var txt = ((RTextScrollPane) tabs.getSelectedComponent()).getTextArea().getText();

        Utils.analize(txt);
        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
            new ConsoleView(console);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void analizedesc() {
        console.setText("");
        var txt = ((RTextScrollPane) tabs.getSelectedComponent()).getTextArea().getText();
        Utils.analizejavacc(txt);

        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
            new ConsoleView(console);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void synErrors() {
    }

    private void semErrors() {
    }

    private void lexErrors() {
    }

    private void closeTab() {
        tabs.remove(tabs.getSelectedIndex());
    }

    private void newTab(int syntax) {
        var index = tabs.getTabCount();
        if (index == 0) {
            tabs.addTab("New File", newTab("", syntax));
        } else {
            tabs.addTab("New File " + tabs.getTabCount(), newTab("", syntax));
        }
        tabs.setSelectedIndex(tabs.getTabCount() - 1);
    }

    private void saveFileAS() {
    }

    private void saveFile() {
    }

    private void openFile(int syntax) throws IOException {
        JFileChooser fileChooser = new JFileChooser();
        var returnVal = fileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            tabs.addTab(fileChooser.getSelectedFile().getName()
                    , newTab(
                            readFile(fileChooser.getSelectedFile().getPath(),
                                    StandardCharsets.UTF_8), syntax));
        }


    }

    public class ConsoleView extends JFrame {
        ConsoleView(JTextArea data) {
            data.setForeground(new Color(0, 200, 0));
            data.setBounds(0, 0, 1072, 680);
            var r = new JScrollPane(data);
            r.setVerticalScrollBarPolicy(
                    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            r.setPreferredSize(new Dimension(1072, 680));
            r.setBounds(8, 8, 1072, 680);
            add(r);
            setLayout(null);
            setTitle("Console");
            setSize(1100, 720);
            setLocationRelativeTo(null);
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setVisible(true);
        }
    }

}
