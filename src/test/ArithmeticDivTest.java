package test;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer;
import org.junit.Test;

import static com.flores8ko.aritlanguage.Utils.ParserExpressionTest;
import static org.junit.Assert.assertEquals;

public class ArithmeticDivTest {
    @Test
    public void IntegerNumeric() throws Exception {
        var ans = ParserExpressionTest("20 / 10.5");
        var expect = new PrimitiveTypoContainer.NUMERIC(20 / 10.5);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }

    @Test
    public void NumericInteger() throws Exception {
        var ans = ParserExpressionTest("20.5 / 10");
        var expect = new PrimitiveTypoContainer.NUMERIC(20.5 / 10);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }

    @Test
    public void NumericNumeric() throws Exception {
        var ans = ParserExpressionTest("30.5 / 20.5");
        var expect = new PrimitiveTypoContainer.NUMERIC(30.5 / 20.5);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }

    @Test
    public void IntegerInteger() throws Exception {
        var ans = ParserExpressionTest("30 / 30");
        var expect = new PrimitiveTypoContainer.INTEGER(30 / 30);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.INTEGER) ans).GetValue());
    }
}
