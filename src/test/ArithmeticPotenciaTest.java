package test;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer;
import org.junit.Test;

import static com.flores8ko.aritlanguage.Utils.ParserExpressionTest;
import static org.junit.Assert.assertEquals;

public class ArithmeticPotenciaTest {
    @Test
    public void IntegerNumeric() throws Exception {
        var ans = ParserExpressionTest("2 ^ 1.2");
        var expect = new PrimitiveTypoContainer.NUMERIC(Math.pow(2, 1.2));
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }

    @Test
    public void NumericInteger() throws Exception {
        var ans = ParserExpressionTest("2.5 ^ 3");
        var expect = new PrimitiveTypoContainer.NUMERIC(Math.pow(2.5, 3));
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }

    @Test
    public void NumericNumeric() throws Exception {
        var ans = ParserExpressionTest("3.5 ^ 2.5");
        var expect = new PrimitiveTypoContainer.NUMERIC(Math.pow(3.5, 2.5));
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }

    @Test
    public void IntegerInteger() throws Exception {
        var ans = ParserExpressionTest("3 ^ 2");
        var expect = new PrimitiveTypoContainer.NUMERIC(Math.pow(3, 2));
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }
}
