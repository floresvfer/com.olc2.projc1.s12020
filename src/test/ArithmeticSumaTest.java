package test;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer;
import org.junit.Test;

import static com.flores8ko.aritlanguage.Utils.ParserExpressionTest;
import static org.junit.Assert.assertEquals;

public class ArithmeticSumaTest {
    @Test
    public void IntegerNumeric() throws Exception {
        var ans = ParserExpressionTest("10 + 10.5");
        var expect = new PrimitiveTypoContainer.NUMERIC(10 + 10.5);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }

    @Test
    public void NumericInteger() throws Exception {
        var ans = ParserExpressionTest("10.5 + 10");
        var expect = new PrimitiveTypoContainer.NUMERIC(10.5 + 10);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }

    @Test
    public void NumericNumeric() throws Exception {
        var ans = ParserExpressionTest("10.5 + 10.5");
        var expect = new PrimitiveTypoContainer.NUMERIC(10.5 + 10.5);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }

    @Test
    public void IntegerInteger() throws Exception {
        var ans = ParserExpressionTest("10 + 10");
        var expect = new PrimitiveTypoContainer.INTEGER(10 + 10);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.INTEGER) ans).GetValue());
    }

    @Test
    public void StringInteger() throws Exception {
        var ans = ParserExpressionTest("\"aver\" + 10");
        var expect = new PrimitiveTypoContainer.STRING("aver10");
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.STRING) ans).GetValue());
    }

    @Test
    public void IntegerString() throws Exception {
        var ans = ParserExpressionTest("10 + \"aver\"");
        var expect = new PrimitiveTypoContainer.STRING("10aver");
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.STRING) ans).GetValue());
    }

    @Test
    public void StringNumeric() throws Exception {
        var ans = ParserExpressionTest("\"aver\" + 10.5");
        var expect = new PrimitiveTypoContainer.STRING("aver10.5");
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.STRING) ans).GetValue());
    }

    @Test
    public void NumericString() throws Exception {
        var ans = ParserExpressionTest("10.5 + \"aver\"");
        var expect = new PrimitiveTypoContainer.STRING("10.5aver");
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.STRING) ans).GetValue());
    }

    @Test
    public void StringBool() throws Exception {
        var ans = ParserExpressionTest("\"aver\" + false");
        var expect = new PrimitiveTypoContainer.STRING("averfalse");
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.STRING) ans).GetValue());
    }

    @Test
    public void BoolString() throws Exception {
        var ans = ParserExpressionTest("true + \"aver\"");
        var expect = new PrimitiveTypoContainer.STRING("trueaver");
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.STRING) ans).GetValue());
    }

    @Test
    public void StringString() throws Exception {
        var ans = ParserExpressionTest("\"hola\" + \"adios\"");
        var expect = new PrimitiveTypoContainer.STRING("holaadios");
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.STRING) ans).GetValue());
    }
}
