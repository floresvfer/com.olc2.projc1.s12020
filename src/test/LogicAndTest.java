package test;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer;
import org.junit.Test;

import static com.flores8ko.aritlanguage.Utils.ParserExpressionTest;
import static org.junit.Assert.assertEquals;

public class LogicAndTest {
    @Test
    public void TrueTrue() throws Exception {
        var ans = ParserExpressionTest("true & true");
        var expect = new PrimitiveTypoContainer.BOOL(true);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }

    @Test
    public void TrueFalse() throws Exception {
        var ans = ParserExpressionTest("true & false");
        var expect = new PrimitiveTypoContainer.BOOL(false);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }

    @Test
    public void FalseTrue() throws Exception {
        var ans = ParserExpressionTest("false & true");
        var expect = new PrimitiveTypoContainer.BOOL(false);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }

    @Test
    public void FalseFalse() throws Exception {
        var ans = ParserExpressionTest("false & false");
        var expect = new PrimitiveTypoContainer.BOOL(false);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }
}
