package test;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer;
import org.junit.Test;

import static com.flores8ko.aritlanguage.Utils.ParserExpressionTest;
import static org.junit.Assert.assertEquals;

public class LogicNotTest {
    @Test
    public void TrueTrue() throws Exception {
        var ans = ParserExpressionTest("!true");
        var expect = new PrimitiveTypoContainer.BOOL(false);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }

    @Test
    public void FalseTrue() throws Exception {
        var ans = ParserExpressionTest("!false");
        var expect = new PrimitiveTypoContainer.BOOL(true);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }
}
