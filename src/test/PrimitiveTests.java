package test;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer;
import org.junit.Test;

import java.util.Random;

import static com.flores8ko.aritlanguage.Utils.ParserExpressionTest;
import static org.junit.Assert.assertEquals;

public class PrimitiveTests {
    @Test
    public void IntegerType() throws Exception {
        var rand = new Random();
        var i = rand.nextInt(1000);
        var ans = ParserExpressionTest(i + "");
        var expect = new PrimitiveTypoContainer.INTEGER(i);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.INTEGER) ans).GetValue());
    }

    @Test
    public void IntegerTypeDefaultValue() throws Exception {
        var ans = ParserExpressionTest("0");
        var expect = new PrimitiveTypoContainer.INTEGER();
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.INTEGER) ans).GetValue());
    }

    @Test
    public void NumericType() throws Exception {
        var rand = new Random();
        var i = rand.nextDouble();
        var ans = ParserExpressionTest(i + "");
        var expect = new PrimitiveTypoContainer.NUMERIC(i);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }
    @Test
    public void NumericTypeDefaultValue() throws Exception {
        var ans = ParserExpressionTest("0.0");
        var expect = new PrimitiveTypoContainer.NUMERIC();
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NUMERIC) ans).GetValue(), 10);
    }


    @Test
    public void BooleanTypeTrue() throws Exception {
        var ans = ParserExpressionTest("true");
        var expect = new PrimitiveTypoContainer.BOOL(true);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }

    @Test
    public void BooleanTypeFalse() throws Exception {
        var ans = ParserExpressionTest("false");
        var expect = new PrimitiveTypoContainer.BOOL(false);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }

    @Test
    public void BooleanTypeDefaultValue() throws Exception {
        var ans = ParserExpressionTest("true");
        var expect = new PrimitiveTypoContainer.BOOL();
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }

    @Test
    public void NullType() throws Exception {
        var ans = ParserExpressionTest("null");
        var expect = new PrimitiveTypoContainer.NULL();
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.NULL) ans).GetValue());
    }


    @Test
    public void StringType() throws Exception {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 50;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        var ans = ParserExpressionTest("\""+generatedString+"\"");
        var expect = new PrimitiveTypoContainer.STRING(generatedString);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.STRING) ans).GetValue());
    }

    @Test
    public void StringTypeDefaultValue() throws Exception {
        var ans = new PrimitiveTypoContainer.STRING();
        var expect = new PrimitiveTypoContainer.NULL();
        assertEquals(expect.GetValue(), ans.GetValue());
    }
}
