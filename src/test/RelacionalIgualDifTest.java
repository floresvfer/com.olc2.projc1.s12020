package test;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer;
import org.junit.Test;

import static com.flores8ko.aritlanguage.Utils.ParserExpressionTest;
import static org.junit.Assert.assertEquals;

public class RelacionalIgualDifTest {
    @Test
    public void IntegerNumeric() throws Exception {
        var ans = ParserExpressionTest("10 == 10.5");
        var expect = new PrimitiveTypoContainer.BOOL(false);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());

        ans = ParserExpressionTest("10 != 10.5");
        expect = new PrimitiveTypoContainer.BOOL(true);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }

    @Test
    public void NumericInteger() throws Exception {
        var ans = ParserExpressionTest("10.5 == 10");
        var expect = new PrimitiveTypoContainer.BOOL(false);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());

        ans = ParserExpressionTest("10.5 != 10");
        expect = new PrimitiveTypoContainer.BOOL(true);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }

    @Test
    public void NumericNumeric() throws Exception {
        var ans = ParserExpressionTest("10.5 == 10.5");
        var expect = new PrimitiveTypoContainer.BOOL(true);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());

        ans = ParserExpressionTest("10.5 != 10.5");
        expect = new PrimitiveTypoContainer.BOOL(false);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }

    @Test
    public void IntegerInteger() throws Exception {
        var ans = ParserExpressionTest("10 == 10");
        var expect = new PrimitiveTypoContainer.BOOL(true);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());

        ans = ParserExpressionTest("10 != 10");
        expect = new PrimitiveTypoContainer.BOOL(false);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }


    @Test
    public void StringString() throws Exception {
        var ans = ParserExpressionTest("\"hola\" == \"adios\"");
        var expect = new PrimitiveTypoContainer.BOOL(false);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());

        ans = ParserExpressionTest("\"hola\" != \"adios\"");
        expect = new PrimitiveTypoContainer.BOOL(true);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.BOOL) ans).GetValue());
    }
}
