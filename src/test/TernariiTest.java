package test;

import com.flores8ko.aritlanguage.PrimitiveTypoContainer;
import org.junit.Test;

import static com.flores8ko.aritlanguage.Utils.ParserExpressionTest;
import static org.junit.Assert.assertEquals;

public class TernariiTest {
    @Test
    public void IntegerInteger() throws Exception {
        var ans = ParserExpressionTest("true ? 5 : 6");
        var expect = new PrimitiveTypoContainer.INTEGER(5);
        assertEquals(expect.typo, ans.typo);
        assertEquals(expect.GetValue(), ((PrimitiveTypoContainer.INTEGER) ans).GetValue());

        ans = ParserExpressionTest("false ? \"hola\" : \"adios\"");
        var expect2 = new PrimitiveTypoContainer.STRING("adios");
        assertEquals(expect2.typo, ans.typo);
        assertEquals(expect2.GetValue(), ((PrimitiveTypoContainer.STRING) ans).GetValue());
    }
}
